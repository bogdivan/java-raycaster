# Java Ray Caster

## Screenshots
<details>
<summary>testMap.png</summary>

![testMap.png](https://gitlab.com/bogdivan/java-raycaster/-/raw/main/img/testMapPNG.png)
</details>

<details>
<summary>testMap.csv</summary>

![testMap.csv](https://gitlab.com/bogdivan/java-raycaster/-/raw/main/img/testMapCSV.png)
</details>

<details>
<summary>w3d.png</summary>

![w3d.png](https://gitlab.com/bogdivan/java-raycaster/-/raw/main/img/w3d.png)
</details>

<details>
<summary>win95.png</summary>

![win95.png](https://gitlab.com/bogdivan/java-raycaster/-/raw/main/img/win95.png)
</details>

## What is it
This is a rendering engine that uses ray casting to draw pseudo 3D graphics. This is not a real 3D game, hence there is no floor and ceiling height variation. The map is grid based. The game outputs using JavaFX Canvas.

## Music attribution
Music used:
* **Bobby Prince** - Get them before they get you (`w3d.mp3`)
* **Bobby Prince** - E3M3 - Deep into the code (`e3m3.mp3`)
* **Bobby Prince** - E2M2 - The Demons from Adrians Pen (`e2m2.mp3`)

## Controls
| Key         | Action         |
|-------------|----------------|
| W           | Move Forward   |
| A           | Move Left      |
| S           | Move Backwards |
| D           | Move Right     |
| Left Arrow  | Rotate Left    |
| Right Arrow | Rotate Right   |

## How to edit a texture
* The texture should be rectangular
* It can be any resolution
### Image textures
If you are editing an image texture file, you can choose any color
### CSV texture
If you are editing a CSV texture file, you should use these indexes as your colors
| Index | Color        | Preview                                                  |
|-------|--------------|----------------------------------------------------------|
| 0     | Black        | ![#000000](https://via.placeholder.com/15/000000?text=+) |
| 1     | Dark Red     | ![#800000](https://via.placeholder.com/15/800000?text=+) |
| 2     | Dark Green   | ![#008000](https://via.placeholder.com/15/008000?text=+) |
| 3     | Dark Yellow  | ![#808000](https://via.placeholder.com/15/808000?text=+) |
| 4     | Dark Blue    | ![#000080](https://via.placeholder.com/15/000080?text=+) |
| 5     | Dark Magenta | ![#800080](https://via.placeholder.com/15/800080?text=+) |
| 6     | Dark Cyan    | ![#008080](https://via.placeholder.com/15/008080?text=+) |
| 7     | Gray         | ![#C0C0C0](https://via.placeholder.com/15/C0C0C0?text=+) |
| 8     | Dark Gray    | ![#808080](https://via.placeholder.com/15/808080?text=+) |
| 9     | Red          | ![#FF0000](https://via.placeholder.com/15/FF0000?text=+) |
| 10    | Green        | ![#00FF00](https://via.placeholder.com/15/00FF00?text=+) |
| 11    | Yellow       | ![#FFFF00](https://via.placeholder.com/15/FFFF00?text=+) |
| 12    | Blue         | ![#0000FF](https://via.placeholder.com/15/0000FF?text=+) |
| 13    | Magenta      | ![#FF00FF](https://via.placeholder.com/15/FF00FF?text=+) |
| 14    | Cyan         | ![#00FFFF](https://via.placeholder.com/15/00FFFF?text=+) |
| 15    | White        | ![#FFFFFF](https://via.placeholder.com/15/FFFFFF?text=+) |

## How to create a map
* The map should be rectangular
* It can be any resolution
### Image map
| Hex       |Preview                                                   | Block        |
|-----------|----------------------------------------------------------|--------------|
| `#000000` | ![#000000](https://via.placeholder.com/15/000000?text=+) | Air          |
| `#0A0ACA` | ![#0A0ACA](https://via.placeholder.com/15/0A0ACA?text=+) | Blue Brick   |
| `#FF3C3C` | ![#FF3C3C](https://via.placeholder.com/15/FF3C3C?text=+) | Brick        |
| `#505050` | ![#505050](https://via.placeholder.com/15/505050?text=+) | Gray Stone   |
| `#B4B4B4` | ![#B4B4B4](https://via.placeholder.com/15/B4B4B4?text=+) | Marble       |
| `#3C8C78` | ![#3C8C78](https://via.placeholder.com/15/3C8C78?text=+) | Metal        |
| `#282828` | ![#282828](https://via.placeholder.com/15/282828?text=+) | Old Stone    |
| `#00FF00` | ![#00FF00](https://via.placeholder.com/15/00FF00?text=+) | Player Start |
| `#825032` | ![#825032](https://via.placeholder.com/15/825032?text=+) | Wood         |
### CSV map
| Index       | Block        |
|-------------|--------------|
| `air`       | Air          |
| `bluebrick` | Blue Brick   |
| `brick`     | Brick        |
| `gstone`    | Gray Stone   |
| `marble`    | Marble       |
| `metal`     | Metal        |
| `ostone`    | Old Stone    |
| `start`     | Player Start |
| `wood`      | Wood         |
