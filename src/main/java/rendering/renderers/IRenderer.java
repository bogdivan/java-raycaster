package rendering.renderers;

import utils.CustomColor;

/**
 * The user of this interface must know how to generate an image
 */
public interface IRenderer {
    /**
     * Generate an image
     * @return Image data
     */
    public abstract CustomColor[][] render(int width, int height);
}
