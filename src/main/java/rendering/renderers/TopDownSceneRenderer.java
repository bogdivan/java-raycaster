package rendering.renderers;

import scene.GameScene;
import scene.Intersection;
import scene.TraceType;
import tile.tileObject.BaseTile;
import utils.CustomColor;

/**
 * A renderer that draws the scene from top down view
 */
public class TopDownSceneRenderer implements IRenderer {
    private static final CustomColor BACKGROUND_COLOR = new CustomColor(0x000000);
    private static final CustomColor PLAYER_SQUARE_COLOR = new CustomColor(0x00ff00);
    private static final double PLAYER_SQUARE_SIZE_TILES = 0.25;
    private static final CustomColor PLAYER_LINE_COLOR = new CustomColor(0x4080ff);
    private static final double PLAYER_LINE_SIZE_TILES = 0.75;
    private static final CustomColor INTERSECTION_SQUARE_COLOR = new CustomColor(0xff008c);
    private static final double INTERSECTION_SQUARE_SIZE = 0.125;

    private GameScene scene;


    /**
     * Initialize the top down renderer
     * @param scene The scene to render
     */
    public TopDownSceneRenderer(GameScene scene) {
        this.scene = scene;
    }

    /**
     * Generate an image from the top down perspective
     */
    @Override
    public CustomColor[][] render(int width, int height) {
        // Create the colors of the image
        CustomColor[][] colors = new CustomColor[height][width];

        // Render map
        int mapWidth = this.scene.getWidth();
        int mapHeight = this.scene.getHeight();
        double tileSize = Math.min((double)height / mapHeight, (double)width / mapWidth);
        drawBackgroundTiles(width, height, colors, tileSize);

        // Render player
        int playerSquareSizePx = (int)(tileSize * PLAYER_SQUARE_SIZE_TILES);
        int playerLineSizePx = (int)(tileSize * PLAYER_LINE_SIZE_TILES);

        double playerAngle = this.scene.getPlayer().getAngle();
        int playerXPx = (int)(this.scene.getPlayer().getX() * tileSize);
        int playerYPx = (int)(this.scene.getPlayer().getY() * tileSize);

        int playerLineEndX = (int)(playerXPx + Math.cos(playerAngle) * playerLineSizePx);
        int playerLineEndY = (int)(playerYPx + Math.sin(playerAngle) * playerLineSizePx);

        drawPlayerLookDirection(colors, tileSize);
        drawLine(colors, PLAYER_LINE_COLOR, playerXPx, playerYPx, playerLineEndX, playerLineEndY, 30, 2);
        drawSquare(colors, PLAYER_SQUARE_COLOR, playerXPx, playerYPx, playerSquareSizePx);

        return colors;
    }

    /**
     * Draw the line representing players looking direction
     * @param colors The colors of the image to draw onto
     * @param tileSize The size of a tile in pixels
     */
    private void drawPlayerLookDirection(CustomColor[][] colors, double tileSize) {
        try {
            Intersection i = this.scene.trace(
                this.scene.getPlayer().getX(),
                this.scene.getPlayer().getY(),
                this.scene.getPlayer().getAngle(),
                TraceType.DRAW
            );
            int checkX = (int)(i.getCoords().getX() * tileSize);
            int checkY = (int)(i.getCoords().getY() * tileSize);
            drawSquare(colors, INTERSECTION_SQUARE_COLOR, checkX, checkY, (int)(tileSize * INTERSECTION_SQUARE_SIZE));
        }
        catch (NullPointerException e) {}
    }

    /**
     * Draw the background representing the scene
     * @param width Width of the target image
     * @param height Height of the target image
     * @param colors The colors of the image to draw onto
     * @param tileSize The size of a tile in pixels
     */
    private void drawBackgroundTiles(int width, int height, CustomColor[][] colors, double tileSize) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                double tileX = x / tileSize;
                double tileY = y / tileSize;

                BaseTile t = this.scene.getTileAt(tileX, tileY);
                if (t.getIsDrawable()) {
                    double deltaX = tileX - this.scene.getPlayer().getX();
                    double deltaY = tileY - this.scene.getPlayer().getY();
                    double distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
                    double brightness = 1 - distance / 10;
                    colors[y][x] = t.sampleTextureAt(tileX, tileY, brightness);
                }
                else {
                    colors[y][x] = BACKGROUND_COLOR;
                }
            }
        }
    }

    /**
     * Draw a square onto a 2D array of colors
     * @param colors Image data to draw onto
     * @param color Color of the square
     * @param centerX Position of the center of the square at coordinate X
     * @param centerY Position of the center of the square at coordinate Y
     * @param size Size of the square
     */
    private static void drawSquare(CustomColor[][] colors, CustomColor color, int centerX, int centerY, int size) {
        int half = size / 2;
        
        for(int y = -half; y < half; y++) {
            for (int x = -half; x < half; x++) {
                try {
                    colors[centerY + y][centerX + x] = color;
                }
                catch (ArrayIndexOutOfBoundsException e) {}
            }
        }
    }

    /**
     * Draw a line onto a 2D array of colors
     * @param colors Image data to draw onto
     * @param color Color of the line
     * @param startX X coordinate of the starting point of the line
     * @param startY Y coordinate of the starting point of the line
     * @param endX X coordinate of the ending point of the line
     * @param endY Y coordinate of the ending point of the line
     * @param samples The number of dots that are drawn to represent a line (More means more dots connected)
     * @param width Width of line
     */
    private static void drawLine(CustomColor[][] colors, CustomColor color, int startX, int startY, int endX, int endY, int samples, int width) {
        double stepX = (double)(endX - startX) / samples;
        double stepY = (double)(endY - startY) / samples;

        for (int i = 0; i < samples; i++) {
            int x = (int)(startX + i * stepX);
            int y = (int)(startY - i * stepY);

            drawSquare(colors, color, x, y, width);
        }
    } 
}
