package rendering.renderers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import scene.GameScene;
import scene.Intersection;
import scene.TraceType;
import scene.WallNormal;
import tile.tileObject.BaseTile;
import utils.CustomColor;

/**
 * A renderer that draws the scene from players POV in first person
 */
public class FirstPersonSceneRenderer implements IRenderer {
    private static final double DRAW_DISTANCE = 64;
    private static final int NUM_CORES = Runtime.getRuntime().availableProcessors();
    private static final HashMap<WallNormal, Double> NORMAL_BRIGHTNESS = new HashMap<WallNormal, Double>() {{
        put(WallNormal.NORTH, 1.0);
        put(WallNormal.SOUTH, 0.6);
        put(WallNormal.WEST, 0.8);
        put(WallNormal.EAST, 0.7);
    }};
    private static final double FOV = Math.toRadians(120);

    private GameScene scene;
    private CustomColor skyColor;
    private CustomColor groundColor;


    /**
     * Initialize the first person renderer
     * @param scene The scene to render
     */
    public FirstPersonSceneRenderer(GameScene scene) {
        this.scene = scene;
        skyColor = scene.getSkyColor();
        groundColor = scene.getGroundColor();
    }

    /**
     * Generate an image from the first person perspective from the player 
     */
    @Override
    public CustomColor[][] render(int width, int height) {
        // Create the colors of the image
        CustomColor[][] colors = new CustomColor[height][width];
        // Precalculate values of rendering
        double halfVFov = Math.atan(Math.tan(FOV / 2) * height / width);
        double perpendicularLength = (width / 2) / Math.tan(FOV / 2);
        int wallHeight = (int)(1 / Math.tan(halfVFov) * height / 2);

        List<RenderChunk> chunks = generateRenderChunks(width);

        // Go through each render chunk
        chunks.parallelStream().forEach((c) -> {
            // Go through each column in the chunk
            for (int x = c.getStart(); x < c.getEnd(); x++) {
                // Get the angle of the 
                double rayPixelOffset = x - width / 2;
                double angleDiff = Math.atan(rayPixelOffset / perpendicularLength);
                // Trace in the direction of the column
                Intersection i = this.scene.trace(
                    this.scene.getPlayer().getX(),
                    this.scene.getPlayer().getY(),
                    this.scene.getPlayer().getAngle() - angleDiff,
                    TraceType.DRAW, DRAW_DISTANCE
                );
                // Wall height is 0 if no intersection, or inverse of the distance if there is
                int perceivedWallHeight = (i == null) ? 0 : computeWallPerceivedHeight(wallHeight, angleDiff, i.getDistance());
                // Pixel where the wall starts or ends
                double wallStart = (height - perceivedWallHeight) / 2.f;
                int wallEnd = (int)wallStart + perceivedWallHeight;
                // Draw the column
                for (int y = 0; y < height; y++) {
                    if (y < wallStart) {
                        colors[y][x] = renderSky();
                    }
                    else if (y < wallEnd) {
                        colors[y][x] = renderWall(i, y - wallStart, perceivedWallHeight);
                    }
                    else {
                        colors[y][x] = renderGround();
                    }
                }
            }
        });

        return colors;
    }

    /**
     * Generate the render chunks depending on the width of the final image and number of CPU cores
     * @param width Width of the final image
     * @return Render chunks
     */
    private static List<RenderChunk> generateRenderChunks(int width) {
        List<RenderChunk> chunks = new ArrayList<RenderChunk>(NUM_CORES);
        int chunkWidth = (int)Math.ceil((double)width / NUM_CORES);
        for (int i = 0; i < NUM_CORES; i++) {
            int chunkStart = i * chunkWidth;
            int chunkEnd = Math.min((i + 1) * chunkWidth, width);
            chunks.add(new RenderChunk(chunkStart, chunkEnd));
        }
        return chunks;
    }
    /**
     * Compute the height of the wall in pixels for a given column
     * @param wallHeight Target height of the wall at the distance of 1 unit
     * @param angleDiff Difference in angle between the current trace and player looking direction (for perspective correction)
     * @param distance Distance in units between the player and the intersection point
     * @return The height of the wall in pixels
     */
    private static int computeWallPerceivedHeight(int wallHeight, double angleDiff, double distance) {
        double correctedDistance = distance * Math.cos(angleDiff);
        return (int)(wallHeight / correctedDistance);
    }

    /**
     * Compute sky color
     * @return the color of the sky
     */
    private CustomColor renderSky() {
        return skyColor;
    }

    /**
     * Compute ground color
     * @return The color of the ground
     */
    private CustomColor renderGround() {
        return groundColor;
    }

    /**
     * Compute wall color
     * @param i Intersection object
     * @param wallPixelY Y pixel of the wall drawing loop
     * @param perceivedWallHeight Target height of the wall
     * @return The color of the wall
     */
    private CustomColor renderWall(Intersection i, double wallPixelY, int perceivedWallHeight) {
        if (i != null) {
            BaseTile t = i.getTile();
            double textureX = findWallTextureX(i);
            double textureY = findWallTextureY(wallPixelY, perceivedWallHeight);
            double distanceBrightness = 1 - i.getDistance() / 10;
            double normalBrightness = NORMAL_BRIGHTNESS.get(i.getNormal());

            return t.sampleTextureAt(textureX, textureY, distanceBrightness * normalBrightness);
        }
        else {
            return skyColor;
        }
    }

    /**
     * Find the sampling point of the wall texture on the X axis
     * @param i Intersection object
     * @return Sampling point on X
     */
    private static double findWallTextureX(Intersection i) {
        // X coordinate of the wall texture is aligned to an axis depending on the facing angle
        switch (i.getNormal()) {
            case SOUTH:
                return i.getCoords().getX();
            case NORTH:
                return -i.getCoords().getX();
            case WEST:
                return i.getCoords().getY();
            case EAST:
                return -i.getCoords().getY();
            default:
                return 0.0;
        }
    }

    /**
     * Find the sampling point of the wall texture on the Y axis
     * @param wallPixelY Y pixel of the wall drawing loop
     * @param perceivedWallHeight Target height of the wall
     * @return Sampling point on Y
     */
    private static double findWallTextureY(double wallPixelY, int perceivedWallHeight) {
        // Y coordinate of the wall texture is stretched to fit the perceived wall height
        return (double)wallPixelY / perceivedWallHeight;
    }
}
