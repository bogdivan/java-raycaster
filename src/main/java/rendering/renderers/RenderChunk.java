package rendering.renderers;

/**
 * Stores the start and the end position to multi thread the rendering
 */
public class RenderChunk {
    private int start;
    private int end;


    /**
     * Initialize the chunk
     * @param start Start pixel
     * @param end End pixel
     */
    public RenderChunk(int start, int end) {
        this.start = start;
        this.end = end;
    }


    /**
     * Get the start pixel of the chunk
     * @return
     */
    public int getStart() {
        return this.start;
    }
    /**
     * Get the end pixel of the chunk
     * @return
     */
    public int getEnd() {
        return this.end;
    }
}
