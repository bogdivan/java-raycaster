package rendering.visualizers;

import utils.CustomColor;


/**
 * The user of this interface must know how to output an image
 */
public interface IVisualizer {
    /**
     * Get the width of the target output image
     * @return
     */
    public abstract int getWidth();
    /**
     * Get the height of the target output image
     * @return
     */
    public abstract int getHeight();
    /**
     * Output an image that is constructed from 2d array of colors
     * @param colors 
     */
    public abstract void visualize(CustomColor[][] colors);
}
