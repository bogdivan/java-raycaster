package rendering.visualizers;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import rendering.other.PixelImage;
import utils.CustomColor;


/**
 * Outputs an image onto JavaFX canvas
 */
public class CanvasVisualizer implements IVisualizer {
    private Canvas canvas;
    private GraphicsContext graphics;


    /**
     * Initialize visualizer that outputs to canvas
     * @param canvas Canvas to output to
     */
    public CanvasVisualizer(Canvas canvas) {
        this.canvas = canvas;
        this.graphics = canvas.getGraphicsContext2D();
    }

    @Override
    public int getWidth() {
        return (int)this.canvas.getWidth();
    }

    @Override
    public int getHeight() {
        return (int)this.canvas.getHeight();
    }

    /**
     * Put the image onto canvas
     */
    @Override
    public void visualize(CustomColor[][] colors) {

        int width = this.getWidth();
        int height = this.getHeight();

        PixelImage image = new PixelImage(width, height);
        image.drawColors(colors);

        this.graphics.drawImage(image, 0, 0);
    }

    /**
     * Resize the canvas
     * @param width new width
     * @param height new height
     */
    public void setSize(int width, int height) {
        this.canvas.setWidth(width);
        this.canvas.setHeight(height);
    }
}
