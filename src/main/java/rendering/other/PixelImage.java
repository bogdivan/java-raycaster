package rendering.other;

import java.nio.IntBuffer;

import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;
import utils.CustomColor;


/**
 * JavaFX image that can display 2d color array
 */
public class PixelImage extends WritableImage {
    private final WritablePixelFormat<IntBuffer> PIXEL_FORMAT = PixelFormat.getIntArgbInstance();


    /**
     * Initialize an image with dimensions
     * @param width Width in pixels
     * @param height Height in pixels
     */
    public PixelImage(int width, int height) {
        super(width, height);
    }


    /**
     * Display 2d colors array
     * @param colors 
     */
    public void drawColors(CustomColor[][] colors) {
        int width = colors[0].length;
        int height = colors.length;
        PixelWriter w = this.getPixelWriter();

        int[] buffer = new int[width * height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int index = y * width + x;
                buffer[index] = colors[y][x].getHex();
            }
        }

        w.setPixels(0, 0, width, height, PIXEL_FORMAT, buffer , 0, width);
    }
}
