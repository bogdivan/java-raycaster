package dataReaders.implemented.tileTexture;

import java.io.File;

import dataReaders.base.IDataParser;
import dataReaders.exceptions.JaggedDataException;
import dataReaders.exceptions.NoDataException;
import dataReaders.inputType.ImageFileReader;
import dataReaders.outputType.ITileTextureReader;
import utils.CustomColor;

/**
 * Reads from a .png file and parses it into tile texture data
 */
public class ImageTileTextureReader extends ImageFileReader implements ITileTextureReader, IDataParser<CustomColor[][], CustomColor[][]> {
    /**
     * Read and image file and parse into tile texture data
     */
    @Override
    public final CustomColor[][] parseFile(File file) {
        String path = file.getPath();

        try {
            return parse(readFile(file));
        }
        catch (NoDataException e) {
            throw new NoDataException(path);
        }
    }

    /**
     * Read converted image data into tile texture data
     */
    @Override
    public final CustomColor[][] parse(CustomColor[][] data) {
        if (data == null || data.length == 0 || data[0].length == 0) {
            // Data is empty
            throw new NoDataException();
        }

        int targetWidth = data[0].length;

        for (int y = 0; y < data.length; y++) {
            if (data[y].length != targetWidth) {
                // Data is jagged
                throw new JaggedDataException("line " + y);
            }
        }

        return data;
    }
}
