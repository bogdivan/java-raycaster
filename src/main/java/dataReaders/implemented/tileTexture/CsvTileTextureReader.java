package dataReaders.implemented.tileTexture;

import java.io.File;

import dataReaders.base.IDataParser;
import dataReaders.exceptions.InvalidValueDataException;
import dataReaders.exceptions.JaggedDataException;
import dataReaders.exceptions.NoDataException;
import dataReaders.inputType.CsvFileReader;
import dataReaders.outputType.ITileTextureReader;
import utils.CustomColor;

/**
 * Reads from a .csv file and transforms it into tile texture data
 */
public class CsvTileTextureReader extends CsvFileReader implements ITileTextureReader, IDataParser<String[][], CustomColor[][]> {
    private static final CustomColor[] LOOK_UP = {
        new CustomColor(0x000000), // 0  - Black
        new CustomColor(0x800000), // 1  - Dark red
        new CustomColor(0x008000), // 2  - Dark green
        new CustomColor(0x808000), // 3  - Dark yellow
        new CustomColor(0x000080), // 4  - Dark blue
        new CustomColor(0x800080), // 5  - Dark magenta
        new CustomColor(0x008080), // 6  - Dark cyan
        new CustomColor(0xc0c0c0), // 7  - Gray
        new CustomColor(0x808080), // 8  - Dark gray
        new CustomColor(0xff0000), // 9  - Red
        new CustomColor(0x00ff00), // 10 - Green
        new CustomColor(0xffff00), // 11 - Yellow
        new CustomColor(0x0000ff), // 12 - Blue
        new CustomColor(0xff00ff), // 13 - Magenta
        new CustomColor(0x00ffff), // 14 - Cyan
        new CustomColor(0xffffff)  // 15 - White
    };


    /**
     * Read a .csv file and parse into tile texture data
     */
    @Override
    public final CustomColor[][] parseFile(File file) {
        String path = file.getPath();

        try {
            return parse(readFile(file));
        }
        catch (InvalidValueDataException e) {
            throw new InvalidValueDataException(e.getType(), e.getReceived(), e.getPosition(), path);
        }
        catch (NoDataException e) {
            throw new NoDataException(path);
        }
    }

    /**
     * Read converted .csv data and parse into tile texture data
     */
    @Override
    public final CustomColor[][] parse(String[][] data) {
        if (data == null || data.length == 0 || data[0].length == 0) {
            // Data is empty
            throw new NoDataException();
        }

        CustomColor[][] colors = new CustomColor[data.length][data[0].length];
        int targetWidth = data[0].length;


        // Go through each string in the CSV file and find corresponding color
        for (int y = 0; y < colors.length; y++) {
            if (data[y].length != targetWidth) {
                // Data is jagged
                throw new JaggedDataException("line " + y);
            }

            for (int x = 0; x < colors[y].length; x++) {
                try {
                    colors[y][x] = parseOne(data[y][x]);
                }
                catch (InvalidValueDataException e) {
                    // Data piece is invalid
                    throw new InvalidValueDataException(e.getType(), e.getReceived(), x + ", " + y);
                }
            }
        }

        // Return it
        return colors;
    }

    /**
     * Parse an index of a color to the color
     * @param val Index of the color
     * @return Color object
     */
    private static final CustomColor parseOne(String val) {
        try {
            int index = Integer.parseInt(val);
            return LOOK_UP[index];
        }
        catch (IndexOutOfBoundsException e) {
            throw new InvalidValueDataException("CSV tile file", val);
        }
        catch (NumberFormatException e) {
            throw new InvalidValueDataException("CSV tile file", val);
        }
    }
}
