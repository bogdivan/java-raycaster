package dataReaders.implemented.sceneInfo;

import java.io.File;

import dataReaders.base.IDataParser;
import dataReaders.exceptions.InvalidValueDataException;
import dataReaders.exceptions.NoDataException;
import dataReaders.inputType.CsvFileReader;
import dataReaders.outputType.ISceneInfoReader;
import javafx.animation.Timeline;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import scene.SceneInfo;
import utils.CustomColor;
import utils.PathUtils;

/**
 * Reads a .csv file and parse it into into scene info
 */
public class CsvSceneInfoReader extends CsvFileReader implements ISceneInfoReader, IDataParser<String[][], SceneInfo> {
    /**
     * Read a .csv file and parse into scene info
     */
    @Override
    public SceneInfo parseFile(File file) {
        String path = file.getParent();

        try {
            return parse(readFile(file));
        }
        catch (InvalidValueDataException e) {
            throw new InvalidValueDataException(e.getType(), e.getReceived(), e.getPosition(), path);
        }
        catch (NoDataException e) {
            throw new NoDataException(path);
        }
    }

    /**
     * Read converted .csv data and parse into scene info
     */
    @Override
    public SceneInfo parse(String[][] data) {
        if (data == null || data.length == 0 || data[0].length == 0) {
            // Data is empty
            throw new NoDataException();
        }
        if (data.length != 1 || data[0].length != 3) {
            // Invalid data format
            throw new InvalidValueDataException("CSV scene info", "file that does not contain a line with 3 elements");
        }

        String[] line = data[0];

        CustomColor skyColor;
        CustomColor groundColor;
        AudioClip music;
        // Sky color
        try {
            skyColor = new CustomColor(Integer.parseInt(line[0], 16));
        }
        catch (NumberFormatException e) {
            throw new InvalidValueDataException("CSV scene info - sky color", line[0], "value 1");
        }
        // Ground color
        try {
            groundColor = new CustomColor(Integer.parseInt(line[1], 16));
        }
        catch (NumberFormatException e) {
            throw new InvalidValueDataException("CSV scene info - ground color", line[1], "value 2");
        }
        // Level music
        File musicFile = new File(PathUtils.getResourcePath(line[2]));
        Media media = new Media(musicFile.toURI().toString());
        music = new AudioClip(media.getSource());
        music.setCycleCount(Timeline.INDEFINITE);

        return new SceneInfo(skyColor, groundColor, music);
    }
    
}
