package dataReaders.implemented.sceneLayout;

import utils.CustomColor;

import java.io.File;

import dataReaders.base.IDataParser;
import dataReaders.exceptions.InvalidValueDataException;
import dataReaders.exceptions.JaggedDataException;
import dataReaders.exceptions.NoDataException;
import dataReaders.inputType.ImageFileReader;
import dataReaders.outputType.ISceneLayoutReader;
import tile.tileObject.TileType;

/**
 * Reads from an image file file and parses it into scene tiles
 */
public class ImageSceneLayoutReader extends ImageFileReader implements ISceneLayoutReader, IDataParser<CustomColor[][], TileType[][]> {
    private static final TileType[] LOOK_UP = TileType.values();


    /**
     * Read an image file and parse into scene tiles
     */
    @Override
    public final TileType[][] parseFile(File file) {
        String path = file.getPath();

        try {
            return parse(readFile(file));
        }
        catch (InvalidValueDataException e) {
            throw new InvalidValueDataException(e.getType(), e.getReceived(), e.getPosition(), path);
        }
        catch (NoDataException e) {
            throw new NoDataException(path);
        }
    }

    /**
     * Read converted image data into scene tiles
     */
    @Override
    public final TileType[][] parse(CustomColor[][] data) {
        if (data == null || data.length == 0 || data[0].length == 0) {
            // Data is empty
            throw new NoDataException();
        }

        TileType[][] tiles = new TileType[data.length][data[0].length];
        int targetWidth = data[0].length;

        // Go through each pixel in the Image file file and find corresponding tile
        for (int y = 0; y < data.length; y++) {
            if (data[y].length != targetWidth) {
                // Data is jagged
                throw new JaggedDataException("line " + y);
            }

            for (int x = 0; x < data[y].length; x++) {
                try {
                    tiles[y][x] = parseOne(data[y][x]);
                }
                catch (InvalidValueDataException e) {
                    // Data piece is invalid
                    throw new InvalidValueDataException(e.getType(), e.getReceived(), x + ", " + y);
                }
            }
        }

        // Return it
        return tiles;
    }

    /**
     * Parse a color of a tile to the tile object
     * @param val Color of the tile
     * @return Tile object
     */
    private final static TileType parseOne(CustomColor val) {
        for (TileType type : LOOK_UP) {
            if (type.getTile().getMapColor().equals(val)) {
                return type;
            }
        }
        throw new InvalidValueDataException("Image scene file", val.toString());
    }
}
