package dataReaders.implemented.sceneLayout;

import java.io.File;

import dataReaders.base.IDataParser;
import dataReaders.exceptions.InvalidValueDataException;
import dataReaders.exceptions.JaggedDataException;
import dataReaders.exceptions.NoDataException;
import dataReaders.inputType.CsvFileReader;
import dataReaders.outputType.ISceneLayoutReader;
import tile.tileObject.TileType;

/**
 * Reads a .csv file and parses it into scene tiles
 */
public class CsvSceneLayoutReader extends CsvFileReader implements ISceneLayoutReader, IDataParser<String[][], TileType[][]> {
    private static final TileType[] LOOK_UP = TileType.values();


    /**
     * Read a .csv file and parse it into scene tiles
     */
    @Override
    public final TileType[][] parseFile(File file) {
        String path = file.getPath();

        try {
            return parse(readFile(file));
        }
        catch (InvalidValueDataException e) {
            throw new InvalidValueDataException(e.getType(), e.getReceived(), e.getPosition(), path);
        }
        catch (NoDataException e) {
            throw new NoDataException(path);
        }
    }

    /**
     * Read converted .csv data and parse it into scene tiles
     */
    @Override
    public final TileType[][] parse(String[][] data) {
        if (data == null || data.length == 0 || data[0].length == 0) {
            // Data is empty
            throw new NoDataException();
        }

        TileType[][] tiles = new TileType[data.length][data[0].length];
        int targetWidth = data[0].length;

        // Go through each string in the CSV file and find corresponding tile
        for (int y = 0; y < data.length; y++) {
            if (data[y].length != targetWidth) {
                // Data is jagged
                throw new JaggedDataException("line " + y);
            }

            for(int x = 0; x < data[y].length; x++) {
                try {
                    tiles[y][x] = parseOne(data[y][x]);
                }
                catch (InvalidValueDataException e) {
                    // Data piece is invalid
                    throw new InvalidValueDataException(e.getType(), e.getReceived(), x + ", " + y);
                }
            }
        }

        // Return it
        return tiles;
    }

    /**
     * Parse a name of a tile to the tile object
     * @param val Name of the tile
     * @return Tile object
     */
    private static final TileType parseOne(String val) {
        for (TileType type : LOOK_UP) {
            if (type.getTile().getId().equals(val)) {
                return type;
            }
        }
        throw new InvalidValueDataException("CSV scene file", val);
    }
}
