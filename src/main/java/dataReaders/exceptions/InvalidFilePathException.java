package dataReaders.exceptions;

/**
 * Exception when the file path is invalid or the file does not exist
 */
public class InvalidFilePathException extends FileLoadingException {
    private String file;


    /**
     * Create the exception of an invalid file path
     * @param file Path specified that is not invalid
     */
    public InvalidFilePathException(String file) {
        super("File path is not valid: " + file);
        this.file = file;
    }


    /**
     * Get the path that was invalid
     * @return
     */
    public String getFile() {
        return this.file;
    }
}
