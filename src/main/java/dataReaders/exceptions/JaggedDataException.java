package dataReaders.exceptions;

/**
 * Exception when an expected rectangular data is jagged
 */
public class JaggedDataException extends FileLoadingException {
    private String position;
    private String file;


    /**
     * Create the exception of a jagged data
     * @param position Position where jagged data was found
     * @param file File where jagged data was found
     */
    public JaggedDataException(String position, String file) {
        super(constructMessage(position, file));
        this.position = position;
        this.file = file;
    }
    /**
     * Create the exception of a jagged data
     * @param position Position where jagged data was found
     */
    public JaggedDataException(String position) {
        this(position, "");
    }


    /**
     * Get position where jagged data was found
     * @return
     */
    public String getPosition() {
        return this.position;
    }
    /**
     * Get file where jagged data was found
     * @return
     */
    public String getFile() {
        return this.file;
    }


    /**
     * Construct the error message from the information given
     * @param position Position where jagged data was found
     * @param file File where jagged data was found
     * @return The error message
     */
    private static String constructMessage(String position, String file) {
        // Initial message
        StringBuilder builder = new StringBuilder("Data is invalid: jagged at" + position);
        // File if valid
        if (file.length() != 0) {
            builder.append(" in " + file);
        }
        // Return final message
        return builder.toString();
    }
}
