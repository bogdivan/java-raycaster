package dataReaders.exceptions;

/**
 * Exception when the file type is not supported
 */
public class UnsupportedFileTypeException extends FileLoadingException {
    /**
     * Create the exception of a file that is unsupported
     * @param path
     */
    public UnsupportedFileTypeException(String path) {
        super("Data is invalid: Unsupported file type for " + path);
    }
}
