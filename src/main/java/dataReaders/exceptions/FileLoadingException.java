package dataReaders.exceptions;

public class FileLoadingException extends RuntimeException {
    public FileLoadingException() {
        super();
    }
    public FileLoadingException(String message) {
        super(message);
    }
}
