package dataReaders.exceptions;

/**
 * Exception when the data contains an invalid value
 */
public class InvalidValueDataException extends FileLoadingException {
    private String type;
    private String received;
    private String position;
    private String file;


    /**
     * Create the exception of an invalid value in data
     * @param type Type of the data expected
     * @param received Data received
     * @param position Position where invalid data was found
     * @param file File where invalid data was found
     */
    public InvalidValueDataException(String type, String received, String position, String file) {
        super(constructMessage(type, received, position, file));
        this.type = type;
        this.received = received;
        this.position = position;
        this.file = file;
    }
    /**
     * Create the exception of an invalid value in data
     * @param type Type of the data expected
     * @param received Value received
     * @param position Position where invalid data was found
     */
    public InvalidValueDataException(String type, String received, String position) {
        this(type, received, position, "");
    }
    /**
     * Create the exception of an invalid value in data
     * @param type Type of the data expected
     * @param received Data received
     */
    public InvalidValueDataException(String type, String received) {
        this(type, received, "", "");
    }


    /**
     * Get the type of expected data
     * @return
     */
    public String getType() {
        return this.type;
    }
    /**
     * Get the value received
     * @return
     */
    public String getReceived() {
        return this.received;
    }
    /**
     * Get the position where data was found
     * @return
     */
    public String getPosition() {
        return this.position;
    }
    /**
     * Get the file where the invalid data was found
     * @return
     */
    public String getFile() {
        return this.file;
    }


    /**
     * Construct the error message from the information given
     * @param type Type of the data expected
     * @param received Data received
     * @param position Position where invalid data was found
     * @param file File where invalid data was found
     * @return The error message
     */
    private static String constructMessage(String type, String received, String position, String file) {
        // Initial message
        StringBuilder builder = new StringBuilder("Data is invalid: unknown value for " + type + " (received: " + received + ")");
        // Position if valid
        if (position.length() != 0) {
            builder.append(" at " + position);
        }
        // File if valid
        if (file.length() != 0) {
            builder.append(" in " + file);
        }
        // Return final message
        return builder.toString();
    }
}
