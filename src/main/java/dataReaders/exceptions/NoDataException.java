package dataReaders.exceptions;

/**
 * Exception when a file contains no data
 */
public class NoDataException extends FileLoadingException {
    private String file;


    /**
     * Create the exception of an empty data file
     * @param file File where no data was found
     */
    public NoDataException(String file) {
        super(constructMessage(file));
        this.file = file;
    }
    /**
     * Create the exception of an empty data file
     */
    public NoDataException() {
        this("");
    }


    /**
     * Get the file where no data was found
     * @return
     */
    public String getFile() {
        return this.file;
    }


    /**
     * Construct the error message from the information given
     * @param file File where no data was found
     * @return The error message
     */
    private static String constructMessage(String file) {
        // Initial message
        StringBuilder builder = new StringBuilder("Data is invalid: empty");
        // File if valid
        if (file.length() != 0) {
            builder.append(" in " + file);
        }
        // Return final message
        return builder.toString();
    }
}
