package dataReaders.outputType;

import dataReaders.base.IFileParser;
import utils.CustomColor;

/**
 * The user of this interface must know how to parse a file into tile texture
 */
public interface ITileTextureReader extends IFileParser<CustomColor[][]> {}
