package dataReaders.outputType;

import dataReaders.base.IFileParser;
import tile.tileObject.TileType;

/**
 * The user of this interface must know how to parse a file into scene tiles
 */
public interface ISceneLayoutReader extends IFileParser<TileType[][]> {}
