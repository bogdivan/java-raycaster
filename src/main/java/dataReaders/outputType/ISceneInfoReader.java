package dataReaders.outputType;

import dataReaders.base.IFileParser;
import scene.SceneInfo;

/**
 * The user of this interface must know how to read a file and transform it to scene info
 */
public interface ISceneInfoReader extends IFileParser<SceneInfo> {}
