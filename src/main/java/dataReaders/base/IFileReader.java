package dataReaders.base;

import java.io.File;

/**
 * The user of this interface must know how read a file and transform it a direct type
 * @param <T> Output type
 */
public interface IFileReader<T> {
    /**
     * Read direct data from a file
     * @param path File path
     * @return Data read from file
     */
    public abstract T readFile(File file);
    /**
     * Get the extension of the file that the parser expects to parse
     * @return
     */
    public abstract String getExpectedFileExtension();
}
