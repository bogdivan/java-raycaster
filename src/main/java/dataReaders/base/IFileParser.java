package dataReaders.base;

import java.io.File;

/**
 * The user of this interface must know how read a file and indirectly transform it into a type that represents a concept
 * @param <T> Output type
 */
public interface IFileParser<T> {
    /**
     * Read and transform data from a file
     * @param path File path
     * @return Data parsed from file
     */
    public abstract T parseFile(File file);
    /**
     * Get the extension of the file that the parser expects to parse
     * @return
     */
    public abstract String getExpectedFileExtension();
}
