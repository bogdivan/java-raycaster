package dataReaders.base;

/**
 * The user of this interface must know how to convert from one data type to another
 * @param <In> Input data type
 * @param <Out> Output data type
 */
public interface IDataParser<In, Out> {
    /**
     * Convert from input to output type
     * @param data Input data
     * @return Parsed data
     */
    public abstract Out parse(In data);
}
