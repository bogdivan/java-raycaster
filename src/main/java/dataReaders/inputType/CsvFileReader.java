package dataReaders.inputType;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import dataReaders.base.IFileReader;
import dataReaders.exceptions.InvalidFilePathException;
import dataReaders.exceptions.NoDataException;

/**
 * Reads a .csv file and transforms it into a 2D String array of data
 */
public class CsvFileReader implements IFileReader<String[][]> {
    /**
     * Read a .csv file and transform to a 2D String array
     */
    @Override
    public String[][] readFile(File file) {
        try {
            // Get the file and read all of its lines
            List<String> lines = Files.readAllLines(file.toPath());
            return parseLines(lines);
        }
        // Catch exception if the file path is not valid
        catch (IOException e) {
            throw new InvalidFilePathException(file.getPath());
        }
    }

    /**
     * Take a list of Strings and transform every line into an array
     * @param lines Individual Strings formatted as .csv file
     * @return Transformed data
     */
    public String[][] parseLines(List<String> lines) {
        if (lines == null || lines.size() == 0) {
            // Data is empty
            throw new NoDataException();
        }

        // Initialize a 2D array with only the y dimension of the .csv file layout
        String[][] data = new String[lines.size()][];

        // Loop through all the lines of the list
        for(int y = 0; y < lines.size(); y++) {
            // Get one line from the file
            String line = lines.get(y);

            // Use regex to eliminate unnecessary spaces
            line = line.replaceAll("\\s*,\\s*", ",");
            String[] columns = line.split(",");

            // Initialize a new array for the x dimension
            data[y] = new String[columns.length];

            // Loop through all the columns in this line (values)
            for(int x = 0; x < data[y].length; x++) {
                // Get the data
                data[y][x] = columns[x];
            }
        }

        // Return it
        return data;
    }

    @Override
    public String getExpectedFileExtension() {
        return ".csv";
    }
}
