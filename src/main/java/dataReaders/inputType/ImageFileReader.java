package dataReaders.inputType;

import utils.CustomColor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

import dataReaders.base.IFileReader;
import dataReaders.exceptions.InvalidFilePathException;

import java.awt.image.BufferedImage;

/**
 * Reads a .png file and transforms it into a 2D array of colors
 */
public class ImageFileReader implements IFileReader<CustomColor[][]> {
    /**
     * Read a .png file and transform to a 2D array of colors
     */
    @Override
    public CustomColor[][] readFile(File file) {
        try {
            // Load the image
            FileInputStream imageFile = new FileInputStream(file);
            BufferedImage image = ImageIO.read(imageFile);

            // Init color 2D array with the size of the image
            CustomColor[][] colors = new CustomColor[(int)image.getHeight()][(int)image.getWidth()];

            // Copy each pixel color to the array
            for (int y = 0; y < colors.length; y++) {
                for (int x = 0; x < colors[0].length; x++) {
                    int col = 0xFFFFFF & image.getRGB(x, y);
                    colors[y][x] = new CustomColor(col);
                }
            }

            // Return it
            return colors;
        }
        // File not fond
        catch (IOException e) {
            throw new InvalidFilePathException(file.getPath());
        }
    }

    @Override
    public String getExpectedFileExtension() {
        return ".png";
    }
}
