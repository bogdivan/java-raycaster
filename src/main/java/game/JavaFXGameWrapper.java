package game;

import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import rendering.visualizers.CanvasVisualizer;

/**
 * Wraps a game and calls its events according to JavaFX framework
 */
public class JavaFXGameWrapper extends AnimationTimer {
    private BaseGame game;
    private Canvas canvas;
    private CanvasVisualizer canvasVisualizer;


    /**
     * Initialize a wrapper object
     * @param game Game to run
     * @param canvas Canvas to render to
     */
    public JavaFXGameWrapper(BaseGame game, Canvas canvas) {
        this.game = game;
        this.canvas = canvas;

        this.canvasVisualizer = new CanvasVisualizer(canvas);

        // Communicate JavaFX key press to the game
        this.canvas.setOnKeyPressed(
            new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent e) {
                    game.keyPressed(e.getCode().toString());
                }
            }
        );
        // Communicate JavaFX key release to the game
        this.canvas.setOnKeyReleased(
            new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent e) {
                    game.keyReleased(e.getCode().toString());
                }
            }
        );
    }


    @Override
    public void handle(long currentTime) {
        this.game.update(canvasVisualizer);
    }
}
