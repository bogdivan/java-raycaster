package game;

import java.io.File;
import java.util.ArrayList;

import rendering.renderers.FirstPersonSceneRenderer;
import rendering.renderers.IRenderer;
import rendering.visualizers.IVisualizer;
import utils.CustomColor;
import utils.TimeInfo;

/**
 * Game of grid-based maze explorer
 */
public class RayCasterGame extends BaseGame {
    private IRenderer renderer;


    /**
     * Initialize ray-caster game
     * @param sceneFilePath
     */
    public RayCasterGame(File sceneFile) {
        super(sceneFile);

        renderer = new FirstPersonSceneRenderer(scene);
    }

    @Override
    protected void handleInput(ArrayList<String> keys) {
        this.scene.getPlayer().updateInput(keys);
    }

    @Override
    protected void tick(TimeInfo time) {
        this.scene.getPlayer().move(time.getDeltaTime());
    }

    @Override
    protected void render(IVisualizer visualizer) {
        int width = visualizer.getWidth();
        int height = visualizer.getHeight();

        CustomColor[][] renderResult = renderer.render(width, height);

        visualizer.visualize(renderResult);
    }
}
