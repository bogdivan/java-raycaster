package game;

import java.io.File;
import java.util.ArrayList;

import rendering.visualizers.IVisualizer;
import scene.GameScene;
import utils.TimeCounter;
import utils.TimeInfo;

/**
 * Encapsulates game logic to make it framework independent
 */
public abstract class BaseGame {
    public GameScene scene;
    private TimeCounter time;
    private ArrayList<String> pressedKeys;


    /**
     * Initialize the game and read the scene file
     * @param sceneFilePath File path
     */
    public BaseGame(File sceneFile) {
        this.scene = new GameScene(sceneFile);
        this.time = new TimeCounter();
        this.pressedKeys = new ArrayList<String>();
    }


    /**
     * Update the game state and render it
     * @param visualizer Visualizer to output to
     */
    public final void update(IVisualizer visualizer) {
        this.handleInput(pressedKeys);
        this.tick(time.update());
        this.render(visualizer);
    }

    /**
     * Simulate a key press
     * @param keyCode JavaFX style key code
     */
    public final void keyPressed(String keyCode) {
        if (! this.pressedKeys.contains(keyCode)) {
            this.pressedKeys.add(keyCode);
        }
    }

    /**
     * Stimulate a key release
     * @param keyCode JavaFX style key code
     */
    public final void keyReleased(String keyCode) {
        if (this.pressedKeys.contains(keyCode)) {
            this.pressedKeys.remove(keyCode);
        }
    }


    /**
     * Update the game objects according to the inputs
     * @param keys List of pressed keys
     */
    protected abstract void handleInput(ArrayList<String> keys);
    /**
     * Update the game objects each frame
     * @param time Time information about the current frame
     */
    protected abstract void tick(TimeInfo time);
    /**
     * Display the game state on the screen
     * @param visualizer Visualizer to output to
     */
    protected abstract void render(IVisualizer visualizer);
}
