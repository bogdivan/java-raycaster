package utils;

/**
 * Represents a RGB color
 */
public class CustomColor {
    private int hex;

    /**
     * Generate a color from hex value (append 0x before value to use hexadecimal notation)
     * @param hex Hex value of color
     */
    public CustomColor(int hex) {
        this.hex = hex;
    }

    /**
     * Generate a color from RGB value
     * @param r Red value (Between 0 and 255)
     * @param g Green value (Between 0 and 255)
     * @param b Blue value (Between 0 and 255)
     */
    public CustomColor(byte r, byte g, byte b) {
        this(toHex(r, g, b));
    }

    /**
     * Get the hex value of the color
     * @return
     */
    public int getHex() {
        return 0xff000000 | this.hex;
    }

    /**
     * Get the red value of the color
     * @return
     */
    public int getR() {
        return (this.hex & 0xff0000) >> 16;
    }

    /**
     * Get the green value of the color
     * @return
     */
    public int getG() {
        return (this.hex & 0x00ff00) >> 8;
    }

    /**
     * Get the blue value of the color
     * @return
     */
    public int getB() {
        return this.hex & 0x0000ff;
    }

    /**
     * Convert a color with RGB input to hexadecimal
     * @param r Red value (Between 0 and 255)
     * @param g Green value (Between 0 and 255)
     * @param b Blue value (Between 0 and 255)
     * @return The hexadecimal value of the color
     */
    public static int toHex(byte r, byte g, byte b) {
        int col = 
            (r & 0xFF) << 16 |
            (g & 0xFF) << 8  |
            (b & 0xFF);

        return col;
    }

    /**
     * Darken a color by a factor with multiplication
     * @param factor 1 is keep the brightness, 0 is completely black, values in between are shades
     * @return A shaded color
     */
    public CustomColor multiply(double factor) {
        factor = Math.max(0, Math.min(1, factor));

        int r = this.getR();
        int g = this.getG();
        int b = this.getB();

        r *= factor;
        g *= factor;
        b *= factor;

        return new CustomColor((byte)r, (byte)g, (byte)b);
    }

    /**
     * Convert a color to a string of its hexidecimal representation
     */
    @Override
    public String toString() {
        return String.format("%06X", this.hex);
    }

    /**
     * Compare an object and return if it's a CustomColor object and has the same values
     */
    @Override
    public boolean equals(Object other) {
        // Same object, equals
        if (this == other) return true;
        // Different type, not equals
        if (! (other instanceof CustomColor)) return false;
        // Compare data
        CustomColor c = (CustomColor)other;
        return this.hex == c.hex;
    }
}
