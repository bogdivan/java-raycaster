package utils;

/**
 * Utility class that counts in game time
 */
public class TimeCounter {
    private final long START_TIME;
    private long previousTime;

    /**
     * Initialize and start a new time counter
     */
    public TimeCounter() {
        this.START_TIME = System.nanoTime();
        this.previousTime = System.nanoTime();
    }

    /**
     * Tick the time counter and get the info
     * @return
     */
    public TimeInfo update() {
        long currentTime = System.nanoTime();
        double deltaTime = (currentTime - this.previousTime) / 1e9;
        double totalTime = (currentTime - this.START_TIME) / 1e9;
        this.previousTime = currentTime;

        return new TimeInfo(totalTime, deltaTime);
    }
}
