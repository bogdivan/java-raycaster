package utils;

/**
 * Utility class to measure the time between 2 points
 */
public class Profiler {
    /**
     * Get the start time of the profiler
     * @return
     */
    public static long startProfile() {
        return System.nanoTime();
    }

    /**
     * Print the difference between the start time and now
     * @param name Name of the profiler to append
     * @param start Start time
     */
    public static void profile(String name, long start) {
        long end = System.nanoTime();
        System.out.println(name + " " + ((end - start) / 1e6));
    }
}
