package utils;

import java.io.File;

import dataReaders.exceptions.InvalidFilePathException;
import dataReaders.exceptions.UnsupportedFileTypeException;
import dataReaders.implemented.sceneInfo.CsvSceneInfoReader;
import dataReaders.implemented.sceneLayout.CsvSceneLayoutReader;
import dataReaders.implemented.sceneLayout.ImageSceneLayoutReader;
import dataReaders.implemented.tileTexture.CsvTileTextureReader;
import dataReaders.implemented.tileTexture.ImageTileTextureReader;
import dataReaders.outputType.ISceneLayoutReader;
import dataReaders.outputType.ISceneInfoReader;
import dataReaders.outputType.ITileTextureReader;
import scene.SceneInfo;
import tile.tileObject.TileType;

/**
 * Utility class to load scene info, layout or tile texture data
 */
public class LoadingUtils {
    private static final ITileTextureReader[] tileTextureReaders;
    private static final ISceneLayoutReader[] sceneLayoutReaders;
    private static final ISceneInfoReader sceneInfoReader;

    static {
        tileTextureReaders = new ITileTextureReader[] {
            new CsvTileTextureReader(),
            new ImageTileTextureReader()
        };
        sceneLayoutReaders = new ISceneLayoutReader[] {
            new CsvSceneLayoutReader(),
            new ImageSceneLayoutReader()
        };
        sceneInfoReader = new CsvSceneInfoReader();

    }

    /**
     * Parse a tile file into data
     * @param file The tile file
     * @return Parsed data
     */
    public static CustomColor[][] loadTileTexture(File file) {
        if (file == null) {
            throw new InvalidFilePathException("No path");
        }
        if (! file.exists()) {
            throw new InvalidFilePathException(file.getAbsolutePath());
        }
        else {
            for (ITileTextureReader r : tileTextureReaders) {
                if (file.getAbsolutePath().contains(r.getExpectedFileExtension())) {
                    return r.parseFile(file);
                }
            }

            throw new UnsupportedFileTypeException(file.getPath());
        }
    }

    /**
    * Parse a tile file into data
     * @param file The tile file path in String type
     * @return Parsed data
     */
    public static CustomColor[][] loadTileTexture(String path) {
        return loadTileTexture(new File(PathUtils.getResourcePath(path)));
    }

    /**
     * Parse a scene file into data
     * @param file The scene file
     * @return Parsed data
     */
    public static TileType[][] loadSceneLayout(File file) {
        if (file == null) {
            throw new InvalidFilePathException("No path");
        }
        if (! file.exists()) {
            throw new InvalidFilePathException(file.getAbsolutePath());
        }
        else {
            for (ISceneLayoutReader r : sceneLayoutReaders) {
                if (file.getAbsolutePath().contains(r.getExpectedFileExtension())) {
                    return r.parseFile(file);
                }
            }

            throw new UnsupportedFileTypeException(file.getPath());
        }
    }

    /**
     * Parse a scene file into data
     * @param path The scene file path as a String type
     * @return Parsed data
     */
    public static TileType[][] loadSceneLayout(String path) {
        return loadSceneLayout(new File(PathUtils.getResourcePath(path)));
    }

    /**
     * Parse a scene's information file into data
     * @param file The scene info file
     * @return Parsed data
     */
    public static SceneInfo loadSceneInfo(File file) {
        if (file == null) {
            throw new InvalidFilePathException("No path");
        }
        if (! file.exists()) {
            throw new InvalidFilePathException(file.getAbsolutePath());
        }
        else {
            return sceneInfoReader.parseFile(file);
        }
    }

    /**
     * Parse a scene's information file into data
     * @param path The scene info file path as a String type
     * @return Parsed data
     */
    public static SceneInfo loadSceneInfo(String path) {
        return loadSceneInfo(new File(PathUtils.getResourcePath(path)));
    }
}
