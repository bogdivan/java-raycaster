package utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import dataReaders.exceptions.InvalidFilePathException;

/**
 * Utility class to get a file path
 */
public class PathUtils {
    private final static ClassLoader l;

    static {
        l = PathUtils.class.getClassLoader();
    }

    /**
     * Convert a relative file path into a URI path
     * @param path File path as a String type
     * @return URI path
     */
    public static String getResourcePath(String path) {
        try {
            URI uri = l.getResource(path).toURI();
            return Paths.get(uri).toString();
        }
        catch (NullPointerException e) {
            throw new InvalidFilePathException(path);
        }
        catch (URISyntaxException e) {
            throw new InvalidFilePathException(path);
        }
    }
}
