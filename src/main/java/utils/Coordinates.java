package utils;

/**
 * Represents the 2D coordinates in the map
 */
public class Coordinates {
    private double x;
    private double y;

    /**
     * Initialize the coordinates
     * @param x X coordinate
     * @param y Y coordinate
     */
    public Coordinates(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Get the x coordinate
     * @return
     */
    public double getX() {
        return this.x;
    }

    /**
     * Get the y coordinate
     * @return
     */
    public double getY() {
        return this.y;
    }

    /**
     * Compare an object and return if it is a Coordinate object and has same values
     */
    @Override
    public boolean equals(Object other) {
        // Same object, equals
        if (this == other) return true;
        // Different type, not equals
        if (! (other instanceof Coordinates)) return false;
        // Compare data
        Coordinates c = (Coordinates)other;
        return this.x == c.x && this.y == c.y;
    }
}
