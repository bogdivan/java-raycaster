package utils;

/**
 * Stores the info about the current time step
 */
public class TimeInfo {
    private double totalTime;
    private double deltaTime;

    /**
     * Initialize the time step
     * @param totalTime Total time elapsed since the start of the timer
     * @param deltaTime Time elapsed since the last tick
     */
    public TimeInfo(double totalTime, double deltaTime) {
        this.totalTime = totalTime;
        this.deltaTime = deltaTime;
    }

    /**
     * Get time elapsed since the start of the timer
     * @return
     */
    public double getTotalTime() {
        return this.totalTime;
    }
    /**
     * Get time elapsed since the last tick
     * @return
     */
    public double getDeltaTime() {
        return this.deltaTime;
    }
}
