package scene;


/**
 * The list of wall normals (where the wall is facing)
 */
public enum WallNormal {
    NORTH,
    SOUTH,
    WEST,
    EAST
}
