package scene;


/**
 * The list of types of the traces (determines which tiles the trace can intersect with)
 */
public enum TraceType {
    /** Trace intersects only with drawable tiles */
    DRAW,
    /** Trace intersects only with collidable tiles */
    COLLISION
}
