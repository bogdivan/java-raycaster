package scene;

import tile.tileObject.BaseTile;
import utils.Coordinates;

/**
 * Stores the info about the intersection
 */
public class Intersection {
    private double distance;
    private Coordinates coords;
    private BaseTile tile;
    private WallNormal normal;


    /**
     * Initialize the intersection
     * @param distance Distance from the start to the end point
     * @param coords Coords of the end point
     * @param tile Tile that was intersected
     * @param normal The normal of the wall tile that was intersected
     */
    public Intersection(double distance, Coordinates coords, BaseTile tile, WallNormal normal) {
        this.distance = distance;
        this.coords = coords;
        this.tile = tile;
        this.normal = normal;
    }


    /**
     * Get the distance from the start to the end point
     * @return
     */
    public double getDistance() {
        return this.distance;
    }
    /**
     * Get the coords of the end point
     * @return
     */
    public Coordinates getCoords() {
        return this.coords;
    }
    /**
     * Get the tile that was intersected
     * @return
     */
    public BaseTile getTile() {
        return this.tile;
    }
    /**
     * Get the normal of the wall tile that was intersected
     * @return
     */
    public WallNormal getNormal() {
        return this.normal;
    }
}
