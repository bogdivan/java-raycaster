package scene;

import javafx.scene.media.AudioClip;
import utils.CustomColor;


/**
 * Stores the info about the scene
 */
public class SceneInfo {
    private CustomColor skyColor;
    private CustomColor groundColor;
    private AudioClip music;


    /**
     * Initialize the info about the scene
     * @param skyColor The color of the sky
     * @param groundColor The color of the ground
     * @param music The music clip that will play in background
     */
    public SceneInfo(CustomColor skyColor, CustomColor groundColor, AudioClip music) {
        this.skyColor = skyColor;
        this.groundColor = groundColor;
        this.music = music;
    }


    /**
     * Get the color of the sky
     * @return
     */
    public CustomColor getSkyColor() {
        return this.skyColor;
    }
    /**
     * Get the color of the ground
     * @return
     */
    public CustomColor getGroundColor() {
        return this.groundColor;
    }
    /**
     * Get the background music clip
     * @return
     */
    public AudioClip getMusic() {
        return this.music;
    }
}
