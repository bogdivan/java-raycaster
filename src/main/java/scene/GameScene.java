package scene;

import java.io.File;

import dataReaders.exceptions.InvalidValueDataException;
import tile.tileObject.*;
import utils.Coordinates;
import utils.CustomColor;
import utils.LoadingUtils;

/**
 * Contains the scene tiles and the player, provides a simple way of tracing rays
 */
public class GameScene {
    private static final double TRACE_EPSILON = 1e-5;

    private SceneInfo info;
    private TileType[][] levelTiles;
    private Player player;


    /**
     * Initialize the scene from the tiles
     * @param sceneFile
     */
    public GameScene(File sceneFile) {
        this.levelTiles = LoadingUtils.loadSceneLayout(sceneFile);
        File infoFile = new File(sceneFile.getPath() + ".info");
        this.info = LoadingUtils.loadSceneInfo(infoFile);

        if (this.checkOnePlayerSpawn()) {
            Coordinates playerCoords = this.getPlayerSpawn();
            this.player = new Player(playerCoords.getX() + 0.5, playerCoords.getY() + 0.5, 90);
            this.info.getMusic().play();
        }
        else {
            throw new InvalidValueDataException("map data", "non 1 player spawn", "unknown", sceneFile.getPath());
        }
    }


    /**
     * Get player object
     * @return
     */
    public Player getPlayer() {
        return this.player;
    }
    /**
     * Get tile at coordinates (returns AIR if outside the map)
     * @param x X coordinate
     * @param y Y coordinate
     * @return Tile object
     */
    public BaseTile getTileAt(double x, double y) {
        try {
            int intX = (int)x;
            int intY = (int)y;

            return this.levelTiles[intY][intX].getTile();
        }
        catch (ArrayIndexOutOfBoundsException e) {
            return TileType.AIR.getTile();
        }
    }
    /**
     * Get the width of the map
     * @return
     */
    public int getWidth() {
        return levelTiles[0].length;
    }
    /**
     * Get the height of the map
     * @return
     */
    public int getHeight() {
        return levelTiles.length;
    }
    /**
     * Get the sky color
     * @return
     */
    public CustomColor getSkyColor() {
        return this.info.getSkyColor();
    }
    /**
     * Get the ground color
     * @return
     */
    public CustomColor getGroundColor() {
        return this.info.getGroundColor();
    }


    /**
     * Trace the ray and find the first intersection
     * @param x X position of the start of the ray
     * @param y Y position of the start of the ray
     * @param angle Angle of the trace
     * @param type Type of the trace (determines what tile intersects with)
     * @param maxDistance Distance when the trace should stop (for optimization reasons)
     * @return Trace info
     */
    public Intersection trace(double x, double y, double angle, TraceType type, double maxDistance) {
        double dirX = Math.cos(angle);
        double dirY = -Math.sin(angle);
        int mapCheckX = (int)x;
        int mapCheckY = (int)y;
        double unitStepX = Math.sqrt(1 + Math.pow((dirY / dirX), 2));
        double unitStepY = Math.sqrt(1 + Math.pow((dirX / dirY), 2));
        double rayLengthX = unitStepX * ((dirX > 0) ? mapCheckX + 1 - x : x - mapCheckX);
        double rayLengthY = unitStepY * ((dirY > 0) ? mapCheckY + 1 - y : y - mapCheckY);
        double totalDistance = 0.0;
        int stepX = (int)Math.signum(dirX);
        int stepY = (int)Math.signum(dirY);

        while (totalDistance < maxDistance) {
            boolean goX = rayLengthX < rayLengthY;
            if (goX) {
                // Step in the x direction
                mapCheckX += stepX;
                totalDistance = rayLengthX;
                rayLengthX += unitStepX;
            }
            else {
                // Step in the y direction
                mapCheckY += stepY;
                totalDistance = rayLengthY;
                rayLengthY += unitStepY;
            }

            BaseTile t = this.getTileAt(mapCheckX, mapCheckY);
            if (t.getIsDrawable()) {
                return generateIntersection(x, y, dirX, dirY, totalDistance, goX, t);
            }
        }

        return null;
    }

    /**
     * Generate intersection info
     * @param x X position of the intersection
     * @param y Y position of the intersection
     * @param dirX Sign of the direction of the ray on X
     * @param dirY Sign of the direction of the ray on Y
     * @param totalDistance Total distance traveled by the ray
     * @param goX If the last movement was along X axis
     * @param t Tile intersected
     * @return Intersection object
     */
    private static Intersection generateIntersection(double x, double y, double dirX, double dirY, double totalDistance, boolean goX, BaseTile t) {
        WallNormal normalX = (dirX > 0) ? WallNormal.WEST : WallNormal.EAST;
        WallNormal normalY = (dirY > 0) ? WallNormal.NORTH : WallNormal.SOUTH;
        Coordinates coords = new Coordinates(
            x + dirX * (totalDistance + TRACE_EPSILON),
            y + dirY * (totalDistance + TRACE_EPSILON)
        );
        WallNormal normal = goX ? normalX : normalY;
        Intersection i = new Intersection(totalDistance, coords, t, normal);
        return i;
    }
    /**
     * Trace the ray and find the first intersection
     * @param x X position of the start of the ray
     * @param y Y position of the start of the ray
     * @param angle Angle of the trace
     * @param type Type of the trace (determines what tile intersects with)
     * @return Trace info
     */
    public Intersection trace(double x, double y, double angle, TraceType type) {
        return this.trace(x, y, angle, type, 64);
    }


    /**
     * Count the number of spawns and check if it is 1
     * @return If the map has a valid number of player spawns
     */
    private boolean checkOnePlayerSpawn() {
        int count = 0;

        for (TileType[] row : this.levelTiles) {
            for (TileType tile : row) {
                if (tile.equals(TileType.PLAYER_START)) {
                    count++;
                }
            }
        }

        return count == 1;
    }

    /**
     * Get the coordinates of the player spawn tile
     * @return Coordinates of a tile with player spawn
     */
    private Coordinates getPlayerSpawn() {
        for (int y = 0; y < this.levelTiles.length; y++) {
            for (int x = 0; x < this.levelTiles[y].length; x++) {
                if (this.levelTiles[y][x].equals(TileType.PLAYER_START)) {
                    return new Coordinates(x, y);
                }
            }
        }

        return null;
    }
}
