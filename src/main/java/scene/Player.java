package scene;

import java.util.ArrayList;


/**
 * Represents the player that can move
 */
public class Player {
    private static final double MOVE_SPEED = 2.0;
    private static final double TURN_SPEED = 2.0; 

    private double x;
    private double y;
    private double angle;
    private double moveInput;
    private double strafeInput;
    private double turnInput;


    /**
     * Initialize the player
     * @param x Initial position X
     * @param y Initial position Y
     * @param angle Initial angle
     */
    public Player(double x, double y, double angle) {
        this.x = x;
        this.y = y;
        this.angle = Math.toRadians(angle);
        this.moveInput = 0.0;
        this.strafeInput = 0.0;
        this.turnInput = 0.0;
    }


    /**
     * Get the X coordinate of the player
     * @return
     */
    public double getX() {
        return this.x;
    }
    /**
     * Get the Y coordinate of the player
     * @return
     */
    public double getY() {
        return this.y;
    }
    /**
     * Get the angle of the player
     * @return
     */
    public double getAngle() {
        return this.angle;
    }


    /**
     * Update the next move from the input
     * @param pressedKeys List containing the keys that are pressed at the moment
     */
    public void updateInput(ArrayList<String> pressedKeys) {
        this.moveInput = getInputFromKeys(pressedKeys, "W", "S");
        this.strafeInput = getInputFromKeys(pressedKeys, "D", "A");
        this.turnInput = getInputFromKeys(pressedKeys, "RIGHT", "LEFT");
    }
    /**
     * Update the location and the rotation of the player from the inputs
     * @param deltaTime Duration of the current frame
     */
    public void move(double deltaTime) {
        this.angle -= TURN_SPEED * deltaTime * this.turnInput;
        this.angle = (this.angle + 2 * Math.PI) % (2 * Math.PI);

        double forwardMovement = deltaTime * MOVE_SPEED * this.moveInput;
        double rightwardMovement = deltaTime * MOVE_SPEED * this.strafeInput;

        this.x += forwardMovement * Math.cos(this.angle) + rightwardMovement * Math.sin(this.angle);
        this.y -= forwardMovement * Math.sin(this.angle) - rightwardMovement * Math.cos(this.angle);
    }


    /**
     * Compute the value of the input from positive and negative key
     * @param pressedKeys List of pressed keys
     * @param positiveKey If this key is pressed, it will be a positive input
     * @param negativeKey If this key is pressed, it will be a negative input
     * @return Input value
     */
    private static int getInputFromKeys(ArrayList<String> pressedKeys, String positiveKey, String negativeKey) {
        boolean positiveInput = pressedKeys.contains(positiveKey);
        boolean negativeInput = pressedKeys.contains(negativeKey);

        if (positiveInput && (! negativeInput)) {
            return 1;
        }
        else if (negativeInput && (! positiveInput)) {
            return -1;
        }
        else {
            return 0;
        }
    }
}
