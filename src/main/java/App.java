import java.io.File;

import dataReaders.exceptions.FileLoadingException;
import game.BaseGame;
import game.JavaFXGameWrapper;
import game.RayCasterGame;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import utils.PathUtils;

public class App extends Application {
    public static void main(String[] args)  {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        initializeWindowProps(stage);
        // Set up window elements
        Group root = new Group();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        Canvas canvas = new Canvas(640, 480);
        canvas.setFocusTraversable(true);
        root.getChildren().add(canvas);
        makeCanvasFullscreen(scene, canvas);

        // Create the game
        File mapFile = askMapFile(stage);
        try {
            BaseGame game = new RayCasterGame(mapFile);
            JavaFXGameWrapper wrapper = new JavaFXGameWrapper(game, canvas);
            wrapper.start();
            stage.show();
        }
        catch (FileLoadingException e) {
            // If there is an error during map load, print user friendly message
            System.out.println(e.getMessage());
        }
    }

    /**
     * Add listener on the resize and adjust canvas size to fit the full screen
     * @param scene Current scene
     * @param canvas Canvas to adjust
     */
    private void makeCanvasFullscreen(Scene scene, Canvas canvas) {
        scene.widthProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> property, Number oldVal, Number newVal) {
                canvas.setWidth(newVal.intValue());
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> property, Number oldVal, Number newVal) {
                canvas.setHeight(newVal.intValue());
            }
        });
    }

    /**
     * Set windows title and icon
     * @param stage Stage to initialize
     */
    private void initializeWindowProps(Stage stage) {
        stage.setTitle("JRCGE");
        stage.getIcons().add(new Image("icon.png"));
    }

    /**
     * Prompt the user with the file choosing dialog to chose a map file
     * @param stage Current stage
     * @return Map file
     */
    public static File askMapFile(Stage stage) {
        FileChooser fc = new FileChooser();
        fc.setTitle("Find map file");
        fc.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("Image map file", "*.png"),
            new FileChooser.ExtensionFilter("CSV map file", "*.csv")
        );
        fc.setInitialDirectory(new File(PathUtils.getResourcePath("scene")));

        File file;

        while (true) {
            file = fc.showOpenDialog(stage);

            if (file != null) {
                return file;
            }
            else {
                System.out.println("Please select a file");
            }
        }
    }
}
