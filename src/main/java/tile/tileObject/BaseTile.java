package tile.tileObject;

import utils.CustomColor;

/**
 * Represents a "block" in the game map using the chosen tile
 */
public abstract class BaseTile {
    private static final int MIN_BRIGHTNESS_LEVEL = 2;
    private final static int BRIGHTNESS_STEPS = 32;
    private final CustomColor[][][] pixels;
    private final boolean isDrawable;
    private final boolean isCollidable;
    private final String id;
    private final CustomColor mapColor;

    /**
     * Create a block
     * @param pixels A 2D array of colors representing the pixels of the texture
     * @param isDrawable A boolean that dictates if the block's texture is to be drawn or not
     * @param isCollidable A boolean that dictates if the player can collide with the wall or not
     * @param id The id of a block
     * @param mapColor The color of the map
     */
    public BaseTile(CustomColor[][] pixels, boolean isDrawable, boolean isCollidable, String id, CustomColor mapColor) {
        this.pixels = computeBrightnessLevels(pixels);
        this.isDrawable = isDrawable;
        this.isCollidable = isCollidable;
        this.id = id;
        this.mapColor = mapColor;
    }

    /**
     * Get a boolean that says if the block is to be drawn or not
     * @return
     */
    public boolean getIsDrawable() {
        return this.isDrawable;
    }

    /**
     * Get a boolean that says if the player can collide with the block or go through it
     * @return 
     */
    public boolean getIsCollidable() {
        return this.isCollidable;
    }

    /**
     * Get the id of a block
     * @return
     */
    public String getId() {
        return this.id;
    }

    /**
     * Get the color of the map
     * @return
     */
    public CustomColor getMapColor() {
        return this.mapColor;
    }

    /**
     * Get the texture pixel from the block
     * @param x X coordinate
     * @param y Y coordinate
     * @param brightness Brightness level
     * @return An array of pixels
     */
    public CustomColor sampleTextureAt(double x, double y, double brightness) {
        // Make the x loop between 0 and 1
        x -= (int)x;
        if (x < 0) {
            x++;
        }
        // Make the y loop between 0 and 1
        y -= (int)y;
        if (y < 0) {
            y++;
        }
        // Cap brightness to be between 0 and 1
        brightness = Math.min(1, Math.max(0, brightness));

        int height = this.pixels[0].length;
        int width = this.pixels[0][0].length;
        
        int brightnessLevel = (int)(brightness * (BRIGHTNESS_STEPS - 1));
        int sampleY = (int)(y * height);
        int sampleX = (int)(x * width);

        return pixels[brightnessLevel][sampleY][sampleX];
    }

    /**
     * Get the texture pixel from the block without specifying the brightness
     * @param x X coordinate
     * @param y Y coordinate
     * @return An array of pixels
     */
    public CustomColor sampleTextureAt(double x, double y) {
        // Overloading method to return a block with a brightness of 1
        return this.sampleTextureAt(x, y, 1);
    }

    /**
     * Convert a full-bright texture into multiple shaded ones
     * @param texture An 2D array of colors representing the texture
     * @return A 3D array where the new dimension is the brightness
     */
    private CustomColor[][][] computeBrightnessLevels(CustomColor[][] texture) {
        int height = texture.length;
        int width = texture[0].length;
        CustomColor[][][] brightnessLevels = new CustomColor[BRIGHTNESS_STEPS][height][width];

        // Iterates through every brightness step to calculate the shade of the texture
        for (int b = 0; b < BRIGHTNESS_STEPS; b++) {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    double factor = (double)(b + MIN_BRIGHTNESS_LEVEL) / (BRIGHTNESS_STEPS + MIN_BRIGHTNESS_LEVEL - 1);
                    brightnessLevels[b][y][x] = texture[y][x].multiply(factor);
                }
            }
        }

        return brightnessLevels;
    }
}
