package tile.tileObject;

import tile.tileSet.*;

/**
 * The type of block
 */
public enum TileType {
    AIR         (new AirTile()),
    PLAYER_START(new PlayerStartTile()),
    BLUE_BRICK  (new BlueBrickTile()),
    BRICK       (new BrickTile()),
    GRAY_STONE  (new GrayStoneTile()),
    MARBLE      (new MarbleTile()),
    METAL       (new MetalTile()),
    OLD_STONE   (new OldStoneTile()),
    WOOD        (new WoodTile());

    private final BaseTile tile;

    private TileType(BaseTile tile) {
        this.tile = tile;
    }

    /**
     * Get the tile type
     * @return
     */
    public BaseTile getTile() {
        return this.tile;
    }
}
