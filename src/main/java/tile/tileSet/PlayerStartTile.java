package tile.tileSet;

import tile.tileObject.BaseTile;
import utils.CustomColor;
import utils.LoadingUtils;

/**
 * Represents the player's starting point
 */
public class PlayerStartTile extends BaseTile {
    public PlayerStartTile() {
        super(
            LoadingUtils.loadTileTexture("tile/error.csv"),
            false, false,
            "start", new CustomColor(0x00ff00)
        );
    }
}
