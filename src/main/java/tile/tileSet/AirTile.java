package tile.tileSet;

import tile.tileObject.BaseTile;
import utils.CustomColor;
import utils.LoadingUtils;

/**
 * Represents an empty block
 */
public class AirTile extends BaseTile {
    public AirTile() {
        super(
            LoadingUtils.loadTileTexture("tile/error.csv"),
            false, false,
            "air", new CustomColor(0x000000)
        );
    }
}
