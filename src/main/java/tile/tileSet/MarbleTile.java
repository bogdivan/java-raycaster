package tile.tileSet;

import tile.tileObject.BaseTile;
import utils.CustomColor;
import utils.LoadingUtils;

/**
 * Represents a marble block
 */
public class MarbleTile extends BaseTile {
    public MarbleTile() {
        super(
            LoadingUtils.loadTileTexture("tile/marble.png"),
            true, true,
            "marble", new CustomColor(0xb4b4b4)
        );
    }
}
