package tile.tileSet;

import tile.tileObject.BaseTile;
import utils.CustomColor;
import utils.LoadingUtils;

/**
 * Represents a metal block
 */
public class MetalTile extends BaseTile {
    public MetalTile() {
        super(
            LoadingUtils.loadTileTexture("tile/metal.png"),
            true, true,
            "metal", new CustomColor(0x3c8c78)
        );
    }
}
