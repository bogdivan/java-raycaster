package tile.tileSet;

import tile.tileObject.BaseTile;
import utils.CustomColor;
import utils.LoadingUtils;

/**
 * Represents an old stone block
 */
public class OldStoneTile extends BaseTile {
    public OldStoneTile() {
        super(
            LoadingUtils.loadTileTexture("tile/oldstone.png"),
            true, true,
            "ostone", new CustomColor(0x282828)
        );
    }
}
