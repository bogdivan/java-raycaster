package tile.tileSet;

import tile.tileObject.BaseTile;
import utils.CustomColor;
import utils.LoadingUtils;

/**
 * Represents a wood block
 */
public class WoodTile extends BaseTile {
    public WoodTile() {
        super(
            LoadingUtils.loadTileTexture("tile/wood.png"),
            true, true,
            "wood", new CustomColor(0x825032)
        );
    }
}
