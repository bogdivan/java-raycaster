package tile.tileSet;

import tile.tileObject.BaseTile;
import utils.CustomColor;
import utils.LoadingUtils;

/**
 * Represents a brick block
 */
public class BrickTile extends BaseTile {
    public BrickTile() {
        super(
            LoadingUtils.loadTileTexture("tile/brick.csv"),
            true, true,
            "brick", new CustomColor(0xff3c3c)
        );
    }
}
