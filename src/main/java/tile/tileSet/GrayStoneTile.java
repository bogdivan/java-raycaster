package tile.tileSet;

import tile.tileObject.BaseTile;
import utils.CustomColor;
import utils.LoadingUtils;

/**
 * Represents a gray stone block
 */
public class GrayStoneTile extends BaseTile {
    public GrayStoneTile() {
        super(
            LoadingUtils.loadTileTexture("tile/graystone.png"),
            true, true,
            "gstone", new CustomColor(0x505050)
        );
    }
}
