package tile.tileSet;

import tile.tileObject.BaseTile;
import utils.CustomColor;
import utils.LoadingUtils;

/**
 * Represents a blue brick block
 */
public class BlueBrickTile extends BaseTile{
    public BlueBrickTile() {
        super(
            LoadingUtils.loadTileTexture("tile/bluebrick.png"),
            true, true,
            "bluebrick", new CustomColor(0x0a0ac8)
        );
    }
}
