package test.java.rendering;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.Test;
import rendering.renderers.RenderChunk;

public class TEST_RenderChunk {
    
    @Test
    public void testGetStart() {
        RenderChunk rc = new RenderChunk(1, 2);
        assertEquals(1, rc.getStart());
    }

    @Test
    public void testGetEnd() {
        RenderChunk rc = new RenderChunk(1, 2);
        assertEquals(2, rc.getEnd());
    }
}
