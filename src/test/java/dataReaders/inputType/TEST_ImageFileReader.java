package test.java.dataReaders.inputType;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

import dataReaders.exceptions.InvalidFilePathException;
import dataReaders.inputType.ImageFileReader;
import utils.CustomColor;
import utils.PathUtils;

public class TEST_ImageFileReader {
    @Test
    public void testCanBeConstructed() {
        new ImageFileReader();
    }


    @Test
    public void testCanReadValidFile() {
        ImageFileReader r = new ImageFileReader();
        CustomColor[][] data = r.readFile(new File(PathUtils.getResourcePath("test/resources/other/validImage.png")));

        assertEquals(new CustomColor(0x000000), data[0][0]);
        assertEquals(new CustomColor(0xff0000), data[0][1]);
        assertEquals(new CustomColor(0x00ff00), data[1][0]);
        assertEquals(new CustomColor(0xffff00), data[1][1]);
    }

    @Test
    public void testExceptionWhenFileDoesNotExist() {
        ImageFileReader r = new ImageFileReader();
        try {
            r.readFile(new File(PathUtils.getResourcePath("This is not a path")));
            fail("Should throw an exception");
        }
        catch (InvalidFilePathException e) {
            // Correct exception
        }
    }
}
