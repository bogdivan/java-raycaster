package test.java.dataReaders.inputType;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import dataReaders.exceptions.InvalidFilePathException;
import dataReaders.exceptions.NoDataException;
import dataReaders.inputType.CsvFileReader;
import utils.PathUtils;

public class TEST_CsvFileReader {
    @Test
    public void testCanBeConstructed() {
        new CsvFileReader();
    }


    @Test
    public void testCanReadValidData() {
        CsvFileReader c = new CsvFileReader();

        List<String> lines = new ArrayList<String>() {{
            add("value, 1, 2");
            add("0, another, 1");
        }};
        String[][] data = c.parseLines(lines);

        assertEquals("value", data[0][0]);
        assertEquals("1", data[0][1]);
        assertEquals("2", data[0][2]);
        assertEquals("0", data[1][0]);
        assertEquals("another", data[1][1]);
        assertEquals("1", data[1][2]);
    }

    @Test
    public void testExceptionWhenDataHasNoData() {
        CsvFileReader c = new CsvFileReader();
        try {
            c.parseLines(null);
            fail("Should throw an exception");
        }
        catch (NoDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testCanReadJaggedData() {
        CsvFileReader c = new CsvFileReader();

        List<String> lines = new ArrayList<String>() {{
            add("value, 1, 2, jagged");
            add("0, another, 1");
        }};
        String[][] data = c.parseLines(lines);

        assertEquals("value", data[0][0]);
        assertEquals("1", data[0][1]);
        assertEquals("2", data[0][2]);
        assertEquals("jagged", data[0][3]);
        assertEquals("0", data[1][0]);
        assertEquals("another", data[1][1]);
        assertEquals("1", data[1][2]);
    }

    @Test
    public void testCanReadWithAdditionalSpacesData() {
        CsvFileReader c = new CsvFileReader();

        List<String> lines = new ArrayList<String>() {{
            add("value,    1,         2");
            add("0,another,  1");
        }};
        String[][] data = c.parseLines(lines);

        assertEquals("value", data[0][0]);
        assertEquals("1", data[0][1]);
        assertEquals("2", data[0][2]);
        assertEquals("0", data[1][0]);
        assertEquals("another", data[1][1]);
        assertEquals("1", data[1][2]);
    }


    @Test
    public void testCanReadValidFile() {
        CsvFileReader c = new CsvFileReader();
        String[][] data = c.readFile(new File(PathUtils.getResourcePath("test/resources/other/validCsv.csv")));

        assertEquals("1", data[0][0]);
        assertEquals("2", data[0][1]);
        assertEquals("data", data[1][0]);
        assertEquals("csv", data[1][1]);
    }

    @Test
    public void testExceptionWhenFileDoesNotExist() {
        CsvFileReader c = new CsvFileReader();
        try {
            c.readFile(new File(PathUtils.getResourcePath("This is not a path")));
            fail("Should throw an exception");
        }
        catch (InvalidFilePathException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenFileHasNoData() {
        CsvFileReader c = new CsvFileReader();
        try {
            c.readFile(new File(PathUtils.getResourcePath("test/resources/other/invalidCsvNoData.csv")));
            fail("Should throw an exception");
        }
        catch (NoDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testCanReadJaggedFile() {
        CsvFileReader c = new CsvFileReader();
        String[][] data = c.readFile(new File(PathUtils.getResourcePath("test/resources/other/validCsvJagged.csv")));

        assertEquals("1", data[0][0]);
        assertEquals("3", data[0][1]);
        assertEquals("10", data[0][2]);
        assertEquals("7", data[1][0]);
        assertEquals("2", data[1][1]);
        assertEquals("test", data[2][0]);
    }

    @Test
    public void testCanReadWithAdditionalSpacesFinal() {
        CsvFileReader c = new CsvFileReader();
        String[][] data = c.readFile(new File(PathUtils.getResourcePath("test/resources/other/validCsvSpaces.csv")));

        assertEquals("data", data[0][0]);
        assertEquals("spaces", data[0][1]);
        assertEquals("1", data[1][0]);
        assertEquals("2", data[1][1]);
    }

}
