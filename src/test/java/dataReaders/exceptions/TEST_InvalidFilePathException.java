package test.java.dataReaders.exceptions;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import dataReaders.exceptions.InvalidFilePathException;

public class TEST_InvalidFilePathException {
    @Test
    public void testConstructor() {
        InvalidFilePathException e = new InvalidFilePathException("file");
        assertEquals("file", e.getFile());
    }
}
