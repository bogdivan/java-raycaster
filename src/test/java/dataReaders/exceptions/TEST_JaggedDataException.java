package test.java.dataReaders.exceptions;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import dataReaders.exceptions.JaggedDataException;

public class TEST_JaggedDataException {
    @Test
    public void testConstructorPositionFile() {
        JaggedDataException e = new JaggedDataException("position", "file");
        assertEquals("position", e.getPosition());
        assertEquals("file",     e.getFile());
    }

    @Test
    public void testConstructorPosition() {
        JaggedDataException e = new JaggedDataException("position");
        assertEquals("position", e.getPosition());
        assertEquals("",         e.getFile());
    }
}
