package test.java.dataReaders.exceptions;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import dataReaders.exceptions.NoDataException;

public class TEST_NoDataException {
    @Test
    public void testConstructorFile() {
        NoDataException e = new NoDataException("file");
        assertEquals("file", e.getFile());
    }

    @Test
    public void testConstructor() {
        NoDataException e = new NoDataException();
        assertEquals("", e.getFile());
    }
}
