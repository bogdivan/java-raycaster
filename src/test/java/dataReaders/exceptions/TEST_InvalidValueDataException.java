package test.java.dataReaders.exceptions;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import dataReaders.exceptions.InvalidValueDataException;

public class TEST_InvalidValueDataException {
    @Test
    public void testConstructorTypeReceivedPositionFile() {
        InvalidValueDataException e = new InvalidValueDataException("type", "received", "position", "file");
        assertEquals("type",     e.getType());
        assertEquals("received", e.getReceived());
        assertEquals("position", e.getPosition());
        assertEquals("file",     e.getFile());
    }

    @Test
    public void testConstructorTypeReceivedPosition() {
        InvalidValueDataException e = new InvalidValueDataException("type", "received", "position");
        assertEquals("type",     e.getType());
        assertEquals("received", e.getReceived());
        assertEquals("position", e.getPosition());
        assertEquals("",         e.getFile());
    }

    @Test
    public void testConstructorTypeReceived() {
        InvalidValueDataException e = new InvalidValueDataException("type", "received");
        assertEquals("type",     e.getType());
        assertEquals("received", e.getReceived());
        assertEquals("",         e.getPosition());
        assertEquals("",         e.getFile());
    }
}
