package test.java.dataReaders.implemented.sceneInfo;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.Test;

import dataReaders.exceptions.InvalidFilePathException;
import dataReaders.exceptions.InvalidValueDataException;
import dataReaders.exceptions.NoDataException;
import dataReaders.implemented.sceneInfo.CsvSceneInfoReader;
import scene.SceneInfo;
import utils.CustomColor;
import utils.PathUtils;

public class TEST_CsvSceneInfoReader {
    @Test
    public void testCanBeConstructed() {
        new CsvSceneInfoReader();
    }

    @Test
    public void testCanParseValidData() {
        CsvSceneInfoReader r = new CsvSceneInfoReader();
        String[][] data = {
            { "123456", "ffffff", "media/e3m3.mp3" }
        };
        SceneInfo info = r.parse(data);
        assertEquals(new CustomColor(0x123456), info.getSkyColor());
        assertEquals(new CustomColor(0xffffff), info.getGroundColor());
        assertTrue(info.getMusic().getSource().contains("e3m3.mp3"));
    }

    @Test
    public void testExceptionWhenDataHasNoData() {
        CsvSceneInfoReader r = new CsvSceneInfoReader();
        try {
            r.parse(null);
        }
        catch (NoDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenDataHasInvalidValue() {
        CsvSceneInfoReader r = new CsvSceneInfoReader();
        String[][] data = {
            { "123456", "ffffff" }
        };

        try {
            r.parse(data);
            fail("Should throw an exception");
        }
        catch (InvalidValueDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testCanParseValidFile() {
        CsvSceneInfoReader r = new CsvSceneInfoReader();
        SceneInfo info = r.parseFile(new File(PathUtils.getResourcePath("test/resources/scene/validScene.csv.info")));
        assertEquals(new CustomColor(0x123456), info.getSkyColor());
        assertEquals(new CustomColor(0xffffff), info.getGroundColor());
        assertTrue(info.getMusic().getSource().contains("e3m3.mp3"));
    }

    @Test
    public void testExceptionWhenFileDoesNotExist() {
        CsvSceneInfoReader c = new CsvSceneInfoReader();
        try {
           c.parseFile(new File(PathUtils.getResourcePath("This is invalid path")));
           fail("Should throw an exception");
        }
        catch (InvalidFilePathException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenFileHasNoData() {
        CsvSceneInfoReader r = new CsvSceneInfoReader();
        try {
            r.parseFile(new File(PathUtils.getResourcePath("test/resources/scene/invalidSceneInfoNoData.csv.info")));
            fail("Should throw an exception");
        }
        catch (NoDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenFileHasInvalidValue() {
        CsvSceneInfoReader r = new CsvSceneInfoReader();
        try {
            r.parseFile(new File(PathUtils.getResourcePath("test/resources/scene/invalidSceneInfo.csv.info")));
            fail("Should throw an exception");
        }
        catch (InvalidValueDataException e) {
            // Correct exception
        }
    }
}
