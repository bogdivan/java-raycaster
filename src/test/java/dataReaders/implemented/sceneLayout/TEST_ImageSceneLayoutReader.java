package test.java.dataReaders.implemented.sceneLayout;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

import dataReaders.exceptions.InvalidFilePathException;
import dataReaders.exceptions.InvalidValueDataException;
import dataReaders.exceptions.JaggedDataException;
import dataReaders.exceptions.NoDataException;
import dataReaders.implemented.sceneLayout.ImageSceneLayoutReader;
import tile.tileObject.TileType;
import utils.CustomColor;
import utils.PathUtils;

/**
 * ! WARNING: These tests fail due to how junit loads objects. IDK how to fix them
 */
public class TEST_ImageSceneLayoutReader {
    @Test
    public void testCanBeConstructed() {
        new ImageSceneLayoutReader();
    }


    @Test
    public void testCanParseValidData() {
        ImageSceneLayoutReader r = new ImageSceneLayoutReader();
        CustomColor[][] data = {
            { new CustomColor(0x000000), new CustomColor(0x282828) },
            { new CustomColor(0x0a0ac8), new CustomColor(0xb4b4b4) },
        };
        TileType[][] tiles = r.parse(data);

        assertEquals(TileType.AIR,        tiles[0][0]);
        assertEquals(TileType.OLD_STONE,  tiles[0][1]);
        assertEquals(TileType.BLUE_BRICK, tiles[1][0]);
        assertEquals(TileType.MARBLE,     tiles[1][1]);
    }

    @Test
    public void testExceptionWhenDataHasNoData() {
        ImageSceneLayoutReader r = new ImageSceneLayoutReader();
        try {
            r.parse(null);
            fail("Should throw an exception");
        }
        catch (NoDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenDataIsJagged() {
        ImageSceneLayoutReader r = new ImageSceneLayoutReader();
        CustomColor[][] data = {
            { new CustomColor(0x000000), new CustomColor(0x282828), new CustomColor(0x505050) },
            { new CustomColor(0x000000), new CustomColor(0x282828), new CustomColor(0x505050) },
            { new CustomColor(0x0a0ac8), new CustomColor(0xb4b4b4) },
        };

        try {
            r.parse(data);
            fail("Should throw an exception");
        }
        catch (JaggedDataException e) {
            if (! e.getPosition().equals("line 2")) {
                fail("Incorrect line");
            }
        }
    }
    @Test
    public void testExceptionWhenDataHasInvalidValue() {
        ImageSceneLayoutReader r = new ImageSceneLayoutReader();
        CustomColor[][] data = {
            { new CustomColor(0x000000), new CustomColor(0x000001) }
        };

        try {
            r.parse(data);
            fail("Should throw an exception");
        }
        catch (InvalidValueDataException e) {
            if (! e.getPosition().equals("1, 0")) {
                fail("Incorrect position");
            }
        }
    }


    @Test
    public void testCanParseValidFile() {
        ImageSceneLayoutReader r = new ImageSceneLayoutReader();
        TileType[][] tiles = r.parseFile(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png")));

        assertEquals(TileType.AIR,        tiles[0][0]);
        assertEquals(TileType.OLD_STONE,  tiles[0][1]);
        assertEquals(TileType.BLUE_BRICK, tiles[1][0]);
        assertEquals(TileType.MARBLE,     tiles[1][1]);
    }

    @Test
    public void testExceptionWhenFileDoesNotExist() {
        ImageSceneLayoutReader r = new ImageSceneLayoutReader();
        try {
            r.parseFile(new File(PathUtils.getResourcePath("This is not a path")));
            fail("Should throw an exception");
        }
        catch (InvalidFilePathException e) {
            // Correct exception
        }
    }

    @Test
    public void testWhenFileHasInvalidValue() {
        ImageSceneLayoutReader r = new ImageSceneLayoutReader();
        try {
            r.parseFile(new File(PathUtils.getResourcePath("test/resources/scene/invalidSceneInvalidValue.png")));
            fail("Should throw an exception");
        }
        catch (InvalidValueDataException e) {
            if (! e.getPosition().equals("2, 2")) {
                fail("Incorrect position");
            }
        }
    }
}
