package test.java.dataReaders.implemented.sceneLayout;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

import dataReaders.exceptions.InvalidValueDataException;
import dataReaders.exceptions.InvalidFilePathException;
import dataReaders.exceptions.JaggedDataException;
import dataReaders.exceptions.NoDataException;
import dataReaders.implemented.sceneLayout.CsvSceneLayoutReader;
import tile.tileObject.TileType;
import utils.PathUtils;

/**
 * ! WARNING: These tests fail due to how junit loads objects. IDK how to fix them
 */
public class TEST_CsvSceneLayoutReader {
    @Test
    public void testCanBeConstructed() {
        new CsvSceneLayoutReader();
    }


    @Test
    public void testCanParseValidData() {
        CsvSceneLayoutReader c = new CsvSceneLayoutReader();
        String[][] data = {
            { "brick", "gstone" },
            { "bluebrick", "start" }
        };
        TileType[][] tiles = c.parse(data);
        assertEquals(TileType.BRICK, tiles[0][0]);
        assertEquals(TileType.GRAY_STONE, tiles[0][1]);
        assertEquals(TileType.BLUE_BRICK, tiles[1][0]);
        assertEquals(TileType.PLAYER_START, tiles[1][1]);
    }

    @Test
    public void testExceptionWhenDataHasNoData() {
        CsvSceneLayoutReader c = new CsvSceneLayoutReader();
        try {
            c.parse(null);
            fail("Should throw an exception");
        }
        catch (NoDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenDataIsJagged() {
        CsvSceneLayoutReader c = new CsvSceneLayoutReader();
        String[][] data = {
            { "brick", "gstone", "air" },
            { "bluebrick", "start", "air" },
            { "gstone", "ostone" }
        };

        try {
            c.parse(data);
            fail("Should throw an exception");
        }
        catch (JaggedDataException e) {
            if (! e.getPosition().equals("line 2")) {
                fail("Incorrect line");
            }
        }
    }

    @Test
    public void testExceptionWhenDataHasInvalidValue() {
        CsvSceneLayoutReader c = new CsvSceneLayoutReader();
        String[][] data = {
            { "air", "ostone" },
            { "1", "gstone" }
        };

        try {
            c.parse(data);
            fail("Should throw an exception");
        }
        catch (InvalidValueDataException e) {
            if (! e.getPosition().equals("0, 1")) {
                fail("Incorrect position");
            }
        }
    }


    @Test
    public void testCanParseValidFile() {
        CsvSceneLayoutReader c = new CsvSceneLayoutReader();
        TileType[][] tiles = c.parseFile(new File(PathUtils.getResourcePath("test/resources/scene/validScene.csv")));

        assertEquals(TileType.PLAYER_START, tiles[0][0]);
        assertEquals(TileType.BRICK, tiles[0][1]);
        assertEquals(TileType.BLUE_BRICK, tiles[1][0]);
        assertEquals(TileType.GRAY_STONE, tiles[1][1]);
    }

    @Test
    public void testExceptionWhenFileDoesNotExist() {
        CsvSceneLayoutReader c = new CsvSceneLayoutReader();
        try {
           c.parseFile(new File(PathUtils.getResourcePath("This is invalid path")));
           fail("Should throw an exception");
        }
        catch (InvalidFilePathException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenFileHasNoData() {
        CsvSceneLayoutReader c = new CsvSceneLayoutReader();
        try {
            c.parseFile(new File(PathUtils.getResourcePath("test/resources/scene/invalidSceneNoData.csv")));
            fail("Should throw an exception");
        }
        catch (NoDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenFileIsJagged() {
        CsvSceneLayoutReader c = new CsvSceneLayoutReader();
        try {
            c.parseFile(new File(PathUtils.getResourcePath("test/resources/scene/invalidSceneJagged.csv")));
            fail("Should throw an exception");
        }
        catch (JaggedDataException e) {
            if (! e.getPosition().equals("line 1")) {
                fail("Incorrect line");
            }
        }
    }

    @Test
    public void testExceptionWhenFileHasInvalidValue() {
        CsvSceneLayoutReader c = new CsvSceneLayoutReader();
        try {
            c.parseFile(new File(PathUtils.getResourcePath("test/resources/scene/invalidSceneInvalidValue.csv")));
            fail("Should throw an exception");
        }
        catch (InvalidValueDataException e) {
            if (! e.getPosition().equals("1, 0")) {
                fail("Incorrect position");
            }
        }
    }
}
