package test.java.dataReaders.implemented.tileTexture;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

import dataReaders.exceptions.InvalidValueDataException;
import dataReaders.exceptions.InvalidFilePathException;
import dataReaders.exceptions.JaggedDataException;
import dataReaders.exceptions.NoDataException;
import dataReaders.implemented.tileTexture.CsvTileTextureReader;
import utils.CustomColor;
import utils.PathUtils;

public class TEST_CsvTileTextureReader {
    private static final CustomColor[] LOOK_UP = {
        new CustomColor(0x000000), // 0  - Black
        new CustomColor(0x800000), // 1  - Dark red
        new CustomColor(0x008000), // 2  - Dark green
        new CustomColor(0x808000), // 3  - Dark yellow
        new CustomColor(0x000080), // 4  - Dark blue
        new CustomColor(0x800080), // 5  - Dark magenta
        new CustomColor(0x008080), // 6  - Dark cyan
        new CustomColor(0xc0c0c0), // 7  - Gray
        new CustomColor(0x808080), // 8  - Dark gray
        new CustomColor(0xff0000), // 9  - Red
        new CustomColor(0x00ff00), // 10 - Green
        new CustomColor(0xffff00), // 11 - Yellow
        new CustomColor(0x0000ff), // 12 - Blue
        new CustomColor(0xff00ff), // 13 - Magenta
        new CustomColor(0x00ffff), // 14 - Cyan
        new CustomColor(0xffffff)  // 15 - White
    };

    @Test
    public void testCanBeConstructed() {
        new CsvTileTextureReader();
    }


    @Test
    public void testCanParseValidData() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        String[][] data = {
            { "1", "2" },
            { "3", "4" }
        };
        CustomColor[][] tiles = c.parse(data);
        assertEquals(LOOK_UP[1], tiles[0][0]);
        assertEquals(LOOK_UP[2], tiles[0][1]);
        assertEquals(LOOK_UP[3], tiles[1][0]);
        assertEquals(LOOK_UP[4], tiles[1][1]);
    }

    @Test
    public void testExceptionWhenDataHasNoData() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        try {
            c.parse(null);
            fail("Should throw an exception");
        }
        catch (NoDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenDataIsJagged() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        String[][] data = {
            { "1", "2", "5" },
            { "1", "2", "5" },
            { "1", "2", "5" },
            { "1", "2", "5" },
            { "3", "4" },
            { "1", "2", "5" },
        };

        try {
            c.parse(data);
            fail("Should throw an exception");
        }
        catch (JaggedDataException e) {
            if (! e.getPosition().equals("line 4")) {
                fail("Incorrect line");
            }
        }
    }

    @Test
    public void testExceptionWhenDataHasInvalidValueOutOfRange() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        String[][] data = {
            { "255" }
        };

        try {
            c.parse(data);
            fail("Should throw an exception");
        }
        catch (InvalidValueDataException e) {
            if (! e.getPosition().equals("0, 0")) {
                fail("Incorrect position");
            }
        }
    }

    @Test
    public void testExceptionWhenDataHasInvalidValueNotNumber() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        String[][] data = {
            { "2" },
            { "notanumber" },
        };

        try {
            c.parse(data);
            fail("Should throw an exception");
        }
        catch (InvalidValueDataException e) {
            if (! e.getPosition().equals("0, 1")) {
                fail("Incorrect position");
            }
        }
    }


    @Test
    public void testCanParseValidFile() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        CustomColor[][] tiles = c.parseFile(new File(PathUtils.getResourcePath("test/resources/tile/validTile.csv")));

        assertEquals(LOOK_UP[10], tiles[0][0]);
        assertEquals(LOOK_UP[2],  tiles[0][1]);
        assertEquals(LOOK_UP[9],  tiles[1][0]);
        assertEquals(LOOK_UP[14], tiles[1][1]);
    }

    @Test
    public void testExceptionWhenFileDoesNotExist() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        try {
           c.parseFile(new File("This is not a path"));
           fail("Should throw an exception");
        }
        catch (InvalidFilePathException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenFileHasNoData() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        try {
            c.parseFile(new File(PathUtils.getResourcePath("test/resources/tile/invalidTileNoData.csv")));
            fail("Should throw an exception");
        }
        catch (NoDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenFileIsJagged() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        try {
            c.parseFile(new File(PathUtils.getResourcePath("test/resources/tile/invalidTileJagged.csv")));
            fail("Should throw an exception");
        }
        catch (JaggedDataException e) {
            if (! e.getPosition().equals("line 1")) {
                fail("Incorrect line");
            }
        }
    }

    @Test
    public void testExceptionWhenFileHasInvalidValueOutOfRange() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        try {
            c.parseFile(new File(PathUtils.getResourcePath("test/resources/tile/invalidTileInvalidValueRange.csv")));
            fail("Should throw an exception");
        }
        catch (InvalidValueDataException e) {
            if (! e.getPosition().equals("1, 0")) {
                fail("Incorrect position");
            }
        }
    }

    @Test
    public void testExceptionWhenFileHasInvalidValueNotNumber() {
        CsvTileTextureReader c = new CsvTileTextureReader();
        try {
            c.parseFile(new File(PathUtils.getResourcePath("test/resources/tile/invalidTileInvalidValueNotNumber.csv")));
            fail("Should throw an exception");
        }
        catch (InvalidValueDataException e) {
            if (! e.getPosition().equals("1, 1")) {
                fail("Incorrect position");
            }
        }
    }
}
