package test.java.dataReaders.implemented.tileTexture;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

import dataReaders.exceptions.InvalidFilePathException;
import dataReaders.exceptions.JaggedDataException;
import dataReaders.exceptions.NoDataException;
import dataReaders.implemented.tileTexture.ImageTileTextureReader;
import utils.CustomColor;
import utils.PathUtils;

public class TEST_ImageTileTextureReader {
    @Test
    public void testCanBeConstructed() {
        new ImageTileTextureReader();
    }


    @Test
    public void testCanParseValidData() {
        ImageTileTextureReader c = new ImageTileTextureReader();
        CustomColor[][] data = {
            { new CustomColor(0x0a141e), new CustomColor(0x28323c) },
            { new CustomColor(0x46505a), new CustomColor(0x646e78) },
        };
        CustomColor[][] tiles = c.parse(data);
        assertEquals(new CustomColor(0x0a141e), tiles[0][0]);
        assertEquals(new CustomColor(0x28323c), tiles[0][1]);
        assertEquals(new CustomColor(0x46505a), tiles[1][0]);
        assertEquals(new CustomColor(0x646e78), tiles[1][1]);
    }

    @Test
    public void testExceptionWhenDataIsEmpty() {
        ImageTileTextureReader c = new ImageTileTextureReader();
        try {
            c.parse(null);
            fail("Should throw an exception");
        }
        catch (NoDataException e) {
            // Correct exception
        }
    }

    @Test
    public void testExceptionWhenDataIsJagged() {
        ImageTileTextureReader c = new ImageTileTextureReader();
        CustomColor[][] data = {
            { new CustomColor(0x0a141e), new CustomColor(0x28323c) },
            { new CustomColor(0x46505a), new CustomColor(0x646e78), new CustomColor(0x828c96) },
        };

        try {
            c.parse(data);
            fail("Should throw an exception");
        }
        catch (JaggedDataException e) {
            if (! e.getPosition().equals("line 1")) {
                fail("Incorrect line");
            }
        }
    }


    @Test
    public void testCanParseValidFile() {
        ImageTileTextureReader c = new ImageTileTextureReader();
        CustomColor[][] tiles = c.parseFile(new File(PathUtils.getResourcePath("test/resources/tile/validTile.png")));

        assertEquals(new CustomColor(0x000000), tiles[0][0]);
        assertEquals(new CustomColor(0xff0000), tiles[0][1]);
        assertEquals(new CustomColor(0x00ff00), tiles[1][0]);
        assertEquals(new CustomColor(0xffff00), tiles[1][1]);
    }

    @Test
    public void testExceptionWhenFileDoesNotExist() {
        ImageTileTextureReader c = new ImageTileTextureReader();
        try {
           c.parseFile(new File(PathUtils.getResourcePath("This is not a path")));
           fail("Should throw an exception");
        }
        catch (InvalidFilePathException e) {
            // Correct exception
        }
    }

}
