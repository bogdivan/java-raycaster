package test.java.utils;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
import utils.TimeCounter;
import utils.TimeInfo;

public class TEST_TimeCounter {
    
    @Test
    public void testTotalTimeUpdate() {
        TimeCounter t = new TimeCounter();
        try { Thread.sleep(1000); } catch (InterruptedException e) { fail(); };
        TimeInfo ti = t.update();
        assertEquals(1, ti.getTotalTime(), 0.05);
        try { Thread.sleep(2000); } catch (InterruptedException e) { fail(); };
        ti = t.update();
        assertEquals(3, ti.getTotalTime(), 0.05);
    }

    @Test
    public void testDeltaTimeUpdate() {
        TimeCounter t = new TimeCounter();
        try { Thread.sleep(1000); } catch (InterruptedException e) { fail(); };
        TimeInfo ti = t.update();
        assertEquals(1, ti.getDeltaTime(), 0.05);
        try { Thread.sleep(2000); } catch (InterruptedException e) { fail(); };
        ti = t.update();
        assertEquals(2, ti.getDeltaTime(), 0.05);
    }
}