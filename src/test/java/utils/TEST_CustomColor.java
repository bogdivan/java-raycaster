package test.java.utils;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import utils.CustomColor;


public class TEST_CustomColor {
    @Test
    public void testThreeBytes() {
        CustomColor cc = new CustomColor((byte)123, (byte)56, (byte)79);
        assertEquals(123, cc.getR());
        assertEquals(56, cc.getG());
        assertEquals(79, cc.getB());
    }

    @Test
    public void testOneInt() {
        CustomColor cc = new CustomColor(0x7b384f);
        assertEquals(123, cc.getR());
        assertEquals(56, cc.getG());
        assertEquals(79, cc.getB());
    }

    @Test
    public void testEqualsSameObject() {
        CustomColor cc1 = new CustomColor(0x0000ff);
        CustomColor cc2 = cc1;

        assertTrue(cc1.equals(cc2));
    }

    @Test
    public void testEqualsDifferentTypes() {
        CustomColor cc1 = new CustomColor(0x00ff00);
        String fakeColor = "This will fail";

        assertFalse(cc1.equals(fakeColor));
    }

    @Test
    public void testEqualsSameValues() {
        CustomColor cc1 = new CustomColor((byte)255, (byte)128, (byte)0);
        CustomColor cc2 = new CustomColor(0xff8000);

        assertTrue(cc1.equals(cc2));
    }
}
