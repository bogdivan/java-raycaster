package test.java.utils;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

import utils.TimeInfo;

public class TEST_TimeInfo {
    
    @Test
    public void testGetTotalTime() {
        TimeInfo ti = new TimeInfo(3.4, 2.6);
        assertEquals(3.4, ti.getTotalTime());
    }

    @Test
    public void testGetDeltaTime() {
        TimeInfo ti = new TimeInfo(3.4, 2.6);
        assertEquals(2.6, ti.getDeltaTime());
    }
}
