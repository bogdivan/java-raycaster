package test.java.utils;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
import utils.Coordinates;

public class TEST_Coordinates {
    
    @Test
    public void testGetX() {
        Coordinates c = new Coordinates(1.5, 4.3);
        assertEquals(1.5, c.getX());
    }

    @Test
    public void testGetY() {
        Coordinates c = new Coordinates(1.5, 4.3);
        assertEquals(4.3, c.getY());
    }

    @Test
    public void testEquals() {
        Object o = new Object();
        Coordinates c = new Coordinates(2.0, 6.7);
        assertFalse(c.equals(o));
    }

    @Test
    public void testEqualsSameObject() {
        Coordinates c1 = new Coordinates(1,2);
        Coordinates c2 = c1;

        assertTrue(c1.equals(c2));
    }

    @Test
    public void testEqualsDifferentTypes() {
        Coordinates c1 = new Coordinates(1,2);
        String fakeCoordinate = "This will fail";

        assertFalse(c1.equals(fakeCoordinate));
    }

    @Test
    public void testEqualsSameValues() {
        Coordinates c1 = new Coordinates(1,2);
        Coordinates c2 = new Coordinates(1,2);

        assertTrue(c1.equals(c2));
    }
}
