package test.java.scene;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.Test;
import scene.GameScene;
import scene.Intersection;
import scene.TraceType;
import scene.WallNormal;
import tile.tileObject.TileType;
import utils.CustomColor;
import utils.PathUtils;

public class TEST_GameScene {
    
    @Test
    public void testGetPlayer() {
        GameScene gc = new GameScene(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png")));
        assertEquals(0.5, gc.getPlayer().getX());
        assertEquals(0.5, gc.getPlayer().getY());
    }

    @Test
    public void testGetTileAt() {
        GameScene gc = new GameScene(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png")));
        assertEquals(TileType.MARBLE.getTile(), gc.getTileAt(1, 1));
    }

    @Test
    public void testGetHeight() {
        GameScene gc = new GameScene(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png")));
        assertEquals(2, gc.getHeight());
    }

    @Test
    public void testGetWidth() {
        GameScene gc = new GameScene(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png")));
        assertEquals(2, gc.getWidth());
    }

    @Test
    public void testGetSkyColor() {
        GameScene gc = new GameScene(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png")));
        assertEquals(new CustomColor(0xffffff), gc.getSkyColor());
    }

    @Test
    public void testGetGroundColor() {
        GameScene gc = new GameScene(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png")));
        assertEquals(new CustomColor(0x000000), gc.getGroundColor());
    }

    @Test
    public void testTrace() {
        GameScene gc = new GameScene(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png")));
        Intersection i = gc.trace(0.5, 0.5, 0, TraceType.DRAW);
        assertEquals(TileType.OLD_STONE.getTile(), i.getTile());
        assertEquals(1, i.getCoords().getX(), 0.01);
        assertEquals(0.5, i.getCoords().getY(), 0.01);
        assertEquals(WallNormal.WEST, i.getNormal());
        assertEquals(0.5, i.getDistance(), 0.01);
    }
}
