package test.java.scene;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import scene.Intersection;
import scene.WallNormal;
import tile.tileObject.BaseTile;
import utils.Coordinates;
import utils.CustomColor;

public class TEST_Intersection {
    private static CustomColor[][] texture = {
        {
            new CustomColor(0xff0000)
        }
    };
    private static BaseTile tile = new BaseTile(texture, true, true, "test", new CustomColor(0xff0000)) {};


    @Test
    public void testCanBeConstructed() {
        new Intersection(10, new Coordinates(10, 2), tile , WallNormal.WEST);
    }

    @Test
    public void testGetDistance() {
        Intersection i = new Intersection(20, new Coordinates(10, 2), tile , WallNormal.WEST);
        assertEquals(20, i.getDistance());
    }

    @Test
    public void testGetCoords() {
        Intersection i = new Intersection(20, new Coordinates(10, 2), tile , WallNormal.WEST);
        assertEquals(new Coordinates(10, 2), i.getCoords());
    }

    @Test
    public void testGetTile() {
        Intersection i = new Intersection(20, new Coordinates(10, 2), tile , WallNormal.WEST);
        assertEquals(tile, i.getTile());
    }

    @Test
    public void testGetNormal() {
        Intersection i = new Intersection(20, new Coordinates(10, 2), tile , WallNormal.WEST);
        assertEquals(WallNormal.WEST, i.getNormal());
    }
}
