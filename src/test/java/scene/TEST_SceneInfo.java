package test.java.scene;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.Test;

import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import scene.SceneInfo;
import utils.CustomColor;
import utils.PathUtils;

public class TEST_SceneInfo {
    private File f = new File(PathUtils.getResourcePath("media/w3d.mp3"));
    private Media m = new Media(f.toURI().toString());
    private AudioClip audio = new AudioClip(m.getSource());

    @Test
    public void testCanBeConstructed() {
        new SceneInfo(new CustomColor(0x000000), new CustomColor(0xff0000), audio);
    }

    @Test
    public void testGetSkyColor() {
        SceneInfo i = new SceneInfo(new CustomColor(0x000000), new CustomColor(0xff0000), audio);
        assertEquals(new CustomColor(0x000000), i.getSkyColor());
    }

    @Test
    public void testGetGroundColor() {
        SceneInfo i = new SceneInfo(new CustomColor(0x000000), new CustomColor(0xff0000), audio);
        assertEquals(new CustomColor(0xff0000), i.getGroundColor());
    }

    @Test
    public void testGetMusic() {
        SceneInfo i = new SceneInfo(new CustomColor(0x000000), new CustomColor(0xff0000), audio);
        assertTrue(i.getMusic().getSource().contains("w3d.mp3"));
    }
}
