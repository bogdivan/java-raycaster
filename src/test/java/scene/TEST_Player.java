package test.java.scene;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.Test;

import scene.Player;

public class TEST_Player {
    @Test
    public void testCanBeConstructed() {
        new Player(1, 1, 1);
    }

    @Test
    public void testInitialX() {
        Player p = new Player(1, 2, 3);
        assertEquals(1, p.getX());
    }

    @Test
    public void testInitialY() {
        Player p = new Player(1, 2, 3);
        assertEquals(2, p.getY());
    }

    @Test
    public void testInitialAngle() {
        Player p = new Player(1, 2, 30);
        assertEquals(Math.toRadians(30), p.getAngle(), 0.01);
    }

    @Test
    public void testMoveForward() {
        ArrayList<String> input = new ArrayList<String>() {{
            add("W");
        }};
        Player p = new Player(1, 2, 45);
        p.updateInput(input);
        p.move(0.1);
        assertTrue(p.getX() > 1);
        assertTrue(p.getY() < 2);
    }

    @Test
    public void testMoveBackward() {
        ArrayList<String> input = new ArrayList<String>() {{
            add("S");
        }};
        Player p = new Player(1, 2, 45);
        p.updateInput(input);
        p.move(0.1);
        assertTrue(p.getX() < 1);
        assertTrue(p.getY() > 2);
    }

    @Test
    public void testMoveLeftward() {
        ArrayList<String> input = new ArrayList<String>() {{
            add("A");
        }};
        Player p = new Player(1, 2, 45);
        p.updateInput(input);
        p.move(0.1);
        assertTrue(p.getX() < 1);
        assertTrue(p.getY() < 2);
    }

    @Test
    public void testMoveRightward() {
        ArrayList<String> input = new ArrayList<String>() {{
            add("D");
        }};
        Player p = new Player(1, 2, 45);
        p.updateInput(input);
        p.move(0.1);
        assertTrue(p.getX() > 1);
        assertTrue(p.getY() > 2);
    }

    @Test
    public void testTurnRight() {
        ArrayList<String> input = new ArrayList<String>() {{
            add("RIGHT");
        }};
        Player p = new Player(1, 2, 90);
        p.updateInput(input);
        p.move(0.1);
        assertTrue(p.getAngle() < Math.toRadians(90));
    }

    @Test
    public void testTurnLeft() {
        ArrayList<String> input = new ArrayList<String>() {{
            add("LEFT");
        }};
        Player p = new Player(1, 2, 90);
        p.updateInput(input);
        p.move(0.1);
        assertTrue(p.getAngle() > Math.toRadians(90));
    }
}
