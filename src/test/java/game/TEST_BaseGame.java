package test.java.game;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.util.ArrayList;

import org.junit.Test;

import game.BaseGame;
import rendering.visualizers.IVisualizer;
import utils.CustomColor;
import utils.PathUtils;
import utils.TimeInfo;

public class TEST_BaseGame {
    @Test
    public void testCanBeConstructed() {
        new BaseGame(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png"))) {
            @Override
            protected void handleInput(ArrayList<String> keys) {}
            @Override
            protected void tick(TimeInfo time) {}
            @Override
            protected void render(IVisualizer visualizer) {}
        };
    }

    @Test
    public void testCanTick() {
        // Hacky way of making the game able to edit this value
        int[] val = new int[1];

        BaseGame g = new BaseGame(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png"))) {
            @Override
            protected void handleInput(ArrayList<String> keys) {}
            @Override
            protected void tick(TimeInfo time) {
                val[0]++;
            }
            @Override
            protected void render(IVisualizer visualizer) {}
        };

        g.update(null);
        assertEquals(1, val[0]);
    }

    @Test
    public void testCanHandleInputWithKeyPressed() {
        // Hacky way of making the game able to edit this value
        int[] val = new int[1];

        BaseGame g = new BaseGame(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png"))) {
            @Override
            protected void handleInput(ArrayList<String> keys) {
                if (keys.contains("Key")) {
                    val[0]++;
                }
            }
            @Override
            protected void tick(TimeInfo time) {}
            @Override
            protected void render(IVisualizer visualizer) {}
        };

        g.keyPressed("Key");
        g.update(null);
        assertEquals(1, val[0]);
    }

    @Test
    public void testCanHandleInputWithKeyReleased() {
        // Hacky way of making the game able to edit this value
        int[] val = new int[1];

        BaseGame g = new BaseGame(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png"))) {
            @Override
            protected void handleInput(ArrayList<String> keys) {
                if (keys.contains("Key")) {
                    val[0]++;
                }
            }
            @Override
            protected void tick(TimeInfo time) {}
            @Override
            protected void render(IVisualizer visualizer) {}
        };

        g.keyPressed("Key");
        g.update(null);
        g.keyReleased("Key");
        g.update(null);
        assertEquals(1, val[0]);
    }

    @Test
    public void testCanVisualize() {
        // Hacky way of making the game able to edit this value
        int[] val = new int[1];

        IVisualizer v = new IVisualizer() {
            @Override
            public int getWidth() { return 0; }
            @Override
            public int getHeight() { return 0; }
            @Override
            public void visualize(CustomColor[][] colors) {
                val[0]++;
            }
        };

        BaseGame g = new BaseGame(new File(PathUtils.getResourcePath("test/resources/scene/validScene.png"))) {
            @Override
            protected void handleInput(ArrayList<String> keys) {}
            @Override
            protected void tick(TimeInfo time) {}
            @Override
            protected void render(IVisualizer visualizer) {
                visualizer.visualize(null);
            }
        };

        g.update(v);
        assertEquals(1, val[0]);
    }
}
