package test.java.tile.tileObject;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;

import tile.tileObject.BaseTile;
import utils.CustomColor;

public class TEST_BaseTile {
    @Test
    public void testCanBeConstructed() {
        new BaseTile(
            new CustomColor[][] { { new CustomColor(0x000000) } },
            true, true,
            "test", new CustomColor(0xff0000)
        ) {};
    }

    @Test
    public void testGetIsDrawable() {
        BaseTile t1 = new BaseTile(
            new CustomColor[][] { { new CustomColor(0x000000) } },
            true, true,
            "test1", new CustomColor(0xff0000)
        ) {};
        BaseTile t2 = new BaseTile(
            new CustomColor[][] { { new CustomColor(0x000000) } },
            false, true,
            "test2", new CustomColor(0xff0000)
        ) {};

        assertTrue(t1.getIsDrawable());
        assertFalse(t2.getIsDrawable());
    }

    @Test
    public void testGetIsCollidable() {
        BaseTile t1 = new BaseTile(
            new CustomColor[][] { { new CustomColor(0x000000) } },
            true, true,
            "test1", new CustomColor(0xff0000)
        ) {};
        BaseTile t2 = new BaseTile(
            new CustomColor[][] { { new CustomColor(0x000000) } },
            true, false,
            "test2", new CustomColor(0xff0000)
        ) {};

        assertTrue(t1.getIsCollidable());
        assertFalse(t2.getIsCollidable());
    }

    @Test
    public void testGetId() {
        BaseTile t = new BaseTile(
            new CustomColor[][] { { new CustomColor(0x000000) } },
            true, true,
            "test", new CustomColor(0xff0000)
        ) {};
        assertEquals("test", t.getId());
    }

    @Test
    public void testGetMapColor() {
        BaseTile t = new BaseTile(
            new CustomColor[][] { { new CustomColor(0x000000) } },
            true, true,
            "test", new CustomColor(0xff0000)
        ) {};
        assertEquals(new CustomColor(0xff0000), t.getMapColor());
    }

    @Test
    public void testSampleTextureAtInRange() {
        CustomColor[][] texture = {
            { new CustomColor(0x000000), new CustomColor(0xff0000) },
            { new CustomColor(0x00ff00), new CustomColor(0xffff00) }
        };

        BaseTile t = new BaseTile(
            texture,
            true, true,
            "test", new CustomColor(0xff0000)
        ) {};

        assertEquals(new CustomColor(0x000000), t.sampleTextureAt(0,    0));
        assertEquals(new CustomColor(0xff0000), t.sampleTextureAt(0.99, 0));
        assertEquals(new CustomColor(0x00ff00), t.sampleTextureAt(0,    0.99));
        assertEquals(new CustomColor(0xffff00), t.sampleTextureAt(0.99, 0.99));
    }

    @Test
    public void testSampleTextureAtOUtsideRange() {
        CustomColor[][] texture = {
            { new CustomColor(0x000000), new CustomColor(0xff0000) },
            { new CustomColor(0x00ff00), new CustomColor(0xffff00) }
        };

        BaseTile t = new BaseTile(
            texture,
            true, true,
            "test", new CustomColor(0xff0000)
        ) {};

        assertEquals(new CustomColor(0x000000), t.sampleTextureAt(1,    1));
        assertEquals(new CustomColor(0xff0000), t.sampleTextureAt(1.99, 1));
        assertEquals(new CustomColor(0x00ff00), t.sampleTextureAt(1,    1.99));
        assertEquals(new CustomColor(0xffff00), t.sampleTextureAt(1.99, 1.99));
    }

    @Test
    public void testSampleTextureAtNegative() {
        CustomColor[][] texture = {
            { new CustomColor(0x000000), new CustomColor(0xff0000) },
            { new CustomColor(0x00ff00), new CustomColor(0xffff00) }
        };

        BaseTile t = new BaseTile(
            texture,
            true, true,
            "test", new CustomColor(0xff0000)
        ) {};

        assertEquals(new CustomColor(0x000000), t.sampleTextureAt(-1,   -1));
        assertEquals(new CustomColor(0xff0000), t.sampleTextureAt(-2.1, -1));
        assertEquals(new CustomColor(0x00ff00), t.sampleTextureAt(-1,   -2.1));
        assertEquals(new CustomColor(0xffff00), t.sampleTextureAt(-2.1, -2.1));
    }
}
